'''
@author: Jeff Andre <jandre@commoninf.com>
@date: March 29, 2021
'''

import copy
import datetime
import logging
import os.path
import re


class DataSet(object):
    '''
    Metadata defining the checks for a set of data tables
    '''
    logger = logging.getLogger("DataSet")
    
    # Data set types
    DATASET_EHR = 0
    DATASET_POSTGRES = 1

    def __init__(self, name, version, type_, table_name_pat, *tables):
        '''
          name - data set name
          version - data set version
          type_ - data set type (either DATASET_EHR or DATASET_POSTGRES)
          base_pat - reqular expression used to extract the "base" part of table name
          tables - metadata for the tables in this data set
          
        '''
        self.name = name
        self.version = version
        self.type = type_
        self.table_name_pattern = table_name_pat
        self.table_name_regexp = re.compile(table_name_pat)
        self.tables = {}
        self.table_stats = []
        self.logger.info("Creating the '%s' data set" % self.name)
        for table in tables:
            if table.name in self.tables:
                self.logger.error("duplicate table: '%s'" % table.name)
            else:
                self.tables[table.name] = table

    def read_table(self, table_name):
        return
            
    def check_table(self, table_name, check_level, print_results=True, verbose=False):
        # find the base table object for the specified name
        base_name = self.name_to_base_name(table_name)
        if base_name not in self.tables:
            self.logger.error("unable to find table metadata for '%s'" % table_name)
            print("unable to find table metadata for '%s'" % table_name)
            return None, None, None, None, None
        else:
            base_table = self.tables[base_name]
        
        # if table_name is different from the name of the base table
        # make a copy of the base_table metadata
        if table_name != base_name:
            table = copy.deepcopy(base_table)
            table.name = table_name
            table.reset()
        else:
            table = base_table

        short_name = os.path.basename(table_name)
        unique_dict = {}
        row_n = 1
        for items in self.read_table(table_name):
            valid, e2valid, msgs, e2msgs, unique_e, unique_key = table.process_row(items, check_level)
            if not valid:
                for msg in msgs:
                    self.logger.error("%s table, %s" % (short_name, msg))
            if not e2valid:
                for e2msg in e2msgs:
                    self.logger.error("%s table, %s" % (short_name, e2msg))
            if unique_e in ['E1','E2']:
                if unique_key in unique_dict:
                    self.logger.error("%s table, row %s, %s, duplicate key: %s" % (short_name, row_n, unique_e, unique_key))
                    if unique_e == 'E1': table.e1_duplicates +=1
                    if unique_e == 'E2': table.e2_duplicates +=1
                else:
                    unique_dict[unique_key] = unique_key
            row_n +=1
        
        table.update_stored_counts()
        
        # merge the statistics from this table with those of the base table
        if table.name != base_name:
            base_table.merge(table)
            self.table_stats.append(table)
            if print_results:
                self.print_results(table.name,
                                   table.render_results(True, True, True, verbose))
                
        return table.valid_rows, table.error_rows, table.e2error_rows, table.e1_duplicates, table.e2_duplicates
    
    def name_to_base_name(self, name):
        match = self.table_name_regexp.match(name)
        if match is not None:
            return match.group(1)
        else:
            return None

    def print_results(self, name, results):
        if len(results) > 0:
            sep_line = "=" * len(name)
            print(sep_line)
            print(name)
            print(sep_line)
            for line in results:
                print(line)
            print("")
            print("") 
        
    def print_table_stats(self, name, include_field_info=True,
                          include_field_errors=True,
                          include_field_e2errors=True,
                          include_freq_cnts=True,
                          verbose=False):
        table = self.find_table_by_name(name)        
        if table is not None:
            results = table.render_results(include_field_info,
                                                       include_field_errors,
                                                       include_field_e2errors,
                                                       include_freq_cnts,
                                                       verbose)
            self.print_results(name, results)
            
        
    def print_merged_table_stats(self, include_field_info=True,
                                 include_field_errors=True,
                                 include_field_e2errors=True,
                                 include_freq_cnts=True,
                                 verbose=False):
        keys = self.tables.keys()
        skeys = sorted(keys)
        for key in skeys:
            self.print_table_stats(key, include_field_info,
                                   include_field_errors,
                                   include_field_e2errors,
                                   include_freq_cnts,
                                   verbose)

    def reset_table_stats(self, name):
        table = self.find_table_by_name(name)
        if table is not None:
            table.reset()
        
    @staticmethod
    def relative_date(delta_days, format="%Y%m%d"):
        today = datetime.date.today()
        rel_date = today + datetime.timedelta(delta_days)
        return rel_date.strftime(format)
    
    def find_table_by_name(self, name):
        if name in self.tables:
            return self.tables[name]
        else:
            for t in self.table_stats:
                if t.name == name:
                    return t
                
        return None   # did not find the table

        
        

'''
@author: Jeff Andre <jandre@commoninf.com>
@date: March 18, 2021
'''

from checker import CategoryField, DataSet, DateField, NumberField, Table, TextField
import logging, csv, re, datetime
class EhrDataSet(DataSet):
    '''
    classdocs
    '''
    logger = logging.getLogger("EhrDataSet")
    
    def __init__(self, table_name_pat, filespec, table_names):
        '''
        Constructor
        '''
        self.filespec = filespec
        tname = table_names[0].split(".")[0]

        results = self.get_table(filespec, tname)
        super(EhrDataSet, self).__init__("EHR file", "V1.0 file spec",
                                          self.DATASET_EHR, 
                                          table_name_pat, 
                                          results)
    
    def var_e(self, v):
        e = re.findall('E1|E2',v)
        e_level = e[0] if len(e) > 0 else ''
        var = re.split("E1:|E2:", v)[1] if e else v
        return[e_level, var.strip()]

    def isInt(self, n):
        try:
            int(n)
            return True
        except ValueError:
            return False

    def isFloat(self, n):
        try:
            float(n)
            return True
        except ValueError:
            return False

    def isDate(self, d):
        try:
            datetime.datetime.strptime(d, "%Y-%m-%d")
            return True
        except ValueError:
            return False

    def min_max_date(self, date):
        if self.isInt(date):
            return DataSet.relative_date(int(date), "%Y-%m-%d")
        elif self.isDate(date):
            return date
        else:
            return None

    def get_table(self, filespec, table_name):
        with open(filespec) as f:
            fields = []
            for i, r in enumerate(csv.DictReader(f)):
                if not r['#']:
                    break
                field_name = r['Name']
                f_req_e, f_req = self.var_e(r['Req?'])
                f_type_e, f_type = self.var_e(r['Type'])
                f_maxlen_e, f_maxlen = self.var_e(r['Max'])
                f_format_e, f_format = self.var_e(r['Format/Code'])
                f_minval_e, f_minval = self.var_e(r['Min value'])
                f_maxval_e, f_maxval = self.var_e(r['Max value'])
                f_unique_e, f_unique = self.var_e(r['Uniqueness'])
                unique = f_unique == 'Y'
                required = f_req == 'Y'
                if f_type == 'Char':
                    text_format = f_format.replace(" ", "") if f_format else None
                    max_len = int(f_maxlen) if f_maxlen else None
                    text_field = TextField(field_name, required, text_format, True, None, max_len, unique,
                                  f_req_e, f_format_e, None, f_maxlen_e, f_unique_e)
                    fields.append(text_field)
                elif f_type == 'DateTime':
                    min_date = self.min_max_date(f_minval)
                    max_date = self.min_max_date(f_maxval)
                    date_field = DateField(field_name, 
                            required, True, min_date, max_date, unique,
                            f_req_e, f_minval_e, f_maxval_e, f_unique_e)
                    fields.append(date_field)
                elif f_type == 'Float':
                    min_val = float(f_minval) if f_minval and self.isFloat(f_minval) else None
                    max_val = float(f_maxval) if f_maxval and self.isFloat(f_maxval) else None
                    number_field = NumberField(field_name, 
                            required, NumberField.FLOAT, False, min_val, max_val, unique, 
                            f_req_e, f_type_e, f_minval_e, f_maxval_e, f_unique_e)
                    fields.append(number_field)
            return Table(table_name, "", *fields)

    def header_isvalid(self, table_name):
        fdata = open(table_name)
        line = fdata.readline() # read header
        header = line.rstrip("\r\n").split("|")
        fdata.close()
        header_fields = []
        with open(self.filespec) as f:
            for r in csv.DictReader(f):
                header_fields.append(r['Name'])
        return header_fields == header

    def read_table(self, table_name):
        self.logger.debug("reading from table '%s'" % table_name)
        f = open(table_name)
        line = f.readline() # skip header
        line = f.readline()
        while line:
            yield line.rstrip("\r\n").split("|")
            line = f.readline()
        f.close()
        return


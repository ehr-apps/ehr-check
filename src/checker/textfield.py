'''
@author: Jeff Andre <jandre@commoninf.com>
@date: March 19, 2021
'''

from checker import Field
import copy
import re

class TextField(Field):
    '''
    Text field data checks and stats
    '''
    ALPHA = "[a-zA-Z]+"
    ALPHANUM = "[a-zA-Z0-9]+"
    NUM = "[0-9]+"

    def __init__(self, name, 
                 required=False, text_format=None, calc_freq_cnts=True, 
                 min_len=None, max_len=None, unique=None,
                 req_e=None, text_format_e=None, min_len_e=None, max_len_e=None, 
                 unique_e=None):
        '''
        Constructor
        '''
        super(TextField, self).__init__(name, required, calc_freq_cnts)
        self.text_regexp = None      
        self.text_format = text_format  
        if text_format is not None:
            self.text_regexp = re.compile(text_format)
        self.min_len = min_len
        self.max_len = max_len
        self.req_e = req_e
        self.text_format_e = text_format_e
        self.min_len_e = min_len_e
        self.max_len_e = max_len_e
        self.unique = unique
        self.unique_e = unique_e

    def __deepcopy__(self, visit):
        # Needed to workaround an inability to perform deepcopy on the
        # self.text_regexp instance variable
        newTextField = TextField(self.name, self.required, self.text_format,
                                 self.calc_freq_cnts, self.min_len, self.max_len, self.unique,
                                 self.req_e, self.text_format_e, 
                                 self.min_len_e, self.max_len_e, self.unique_e)
        newTextField.valid_cnt = self.valid_cnt
        newTextField.error_cnt = self.error_cnt
        newTextField.empty_cnt = self.empty_cnt
        newTextField.text_len_lwm = self.text_len_lwm
        newTextField.text_len_hwm = self.text_len_hwm
        newTextField.freq_cnts = copy.deepcopy(self.freq_cnts, visit)
        newTextField.errors = copy.deepcopy(self.errors, visit)
        newTextField.e2errors = copy.deepcopy(self.e2errors, visit)
        return newTextField
         
    def merge(self, field):
        super(TextField, self).merge(field)
        
    def is_valid(self, value, strictness=Field.STRICT):
        if strictness >= Field.MODERATE:
            e1 = self.req_e and self.req_e == 'E1'
            e2 = self.req_e and self.req_e == 'E2'
            if self.required and self.is_empty(value) and (e1 or e2):
                msg = "required"
                return not e1, not e2, msg
        
        if self.is_empty(value):
            self.update_text_len_watermarks(value)
            return True, True, None
        
        text_len = len(value) if value else 0
        if strictness >= Field.STRICT:
            e1 = self.min_len_e and self.min_len_e == 'E1'
            e2 = self.min_len_e and self.min_len_e == 'E2'
            if self.min_len is not None and text_len < self.min_len and (e1 or e2):
                msg = "value < minimum length: value: %s, min_len: %d" % (value, self.min_len)
                return not e1, not e2, msg
                
            e1 = self.max_len_e and self.max_len_e == 'E1'
            e2 = self.max_len_e and self.max_len_e == 'E2'
            if self.max_len is not None and text_len > self.max_len and (e1 or e2):
                msg = "value > maximum length: value: %s, max_len: %d" % (value, self.max_len)
                return not e1, not e2, msg
        
        if strictness >= Field.MODERATE and self.text_regexp:
            e1 = self.text_format_e and self.text_format_e == 'E1'
            e2 = self.text_format_e and self.text_format_e == 'E2'
            match = re.match(self.text_regexp, value)
            if match is None and (e1 or e2):
                msg = "no match: value: %s, pattern: %s" % (value, self.text_format)
                return not e1, not e2, msg
        
        self.update_text_len_watermarks(value)
        return True, True, None
        
    def render_metadata(self):
        text_format = self.text_format
        if text_format is not None:
            text_format = "'%s'" % text_format
            
        msg_fmt = "Text, required=%s, calc_freq_cnts=%s, text format=%s, min len=%s, max len=%s"
        s = msg_fmt % (self.required, self.calc_freq_cnts,
                       text_format, self.min_len, self.max_len)
        return [s]

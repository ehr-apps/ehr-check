'''
@author: Jeff Ander <jandre@commoninf.com>
@date: March 29, 2021
'''

from checker import Field
import copy
import re

class NumberField(Field):
    '''
    Number field data checks and stats
    '''

    POSINT = "\+?\d+"
    INT = "[-+]?\d+"
    FLOAT = "[-+]?\d*(\.\d*)?"
    
    def __init__(self, name, required=False, number_format=None, calc_freq_cnts=True, 
                 min_number=None, max_number=None, unique=None,
                 req_e=None, number_format_e=None, 
                 min_number_e=None, max_number_e=None, unique_e=None):
        '''
        Constructor
        '''
        super(NumberField, self).__init__(name, required, calc_freq_cnts)
        self.number_regexp = None      
        self.number_format = number_format  
        if number_format is not None:
            self.number_regexp = re.compile(number_format)
        
        self.min_number = min_number
        self.max_number = max_number
            
        self.number_lwm = None
        self.number_hwm = None

        self.req_e = req_e
        self.number_format_e = number_format_e
        self.min_number_e = min_number_e
        self.max_number_e = max_number_e
        self.unique = unique
        self.unique_e = unique_e
         
    def __deepcopy__(self, visit):
        # Needed to workaround an inability to perform deepcopy on the
        # self.number_regexp instance variable
        newNumberField = NumberField(self.name, self.required, self.number_format,
                                     self.calc_freq_cnts, self.min_number, 
                                     self.max_number, self.unique,
                                     self.req_e, self.number_format_e, 
                                     self.min_number_e, self.max_number_e, 
                                     self.unique_e)
        newNumberField.number_lwm = self.number_lwm
        newNumberField.number_hwm = self.number_hwm
        newNumberField.valid_cnt = self.valid_cnt
        newNumberField.error_cnt = self.error_cnt
        newNumberField.empty_cnt = self.empty_cnt
        newNumberField.text_len_lwm = self.text_len_lwm
        newNumberField.text_len_hwm = self.text_len_hwm
        newNumberField.freq_cnts = copy.deepcopy(self.freq_cnts, visit)
        newNumberField.errors = copy.deepcopy(self.errors, visit)
        newNumberField.e2errors = copy.deepcopy(self.e2errors, visit)
        return newNumberField
         
    def merge(self, field):
        super(NumberField, self).merge(field)
        self.number_lwm = self.min_(self.number_lwm, field.number_lwm)
        self.number_hwm = self.max_(self.number_hwm, field.number_hwm)
        
    def is_valid(self, value, strictness=Field.STRICT):
        if strictness >= Field.MODERATE:
            e1 = self.req_e and self.req_e == 'E1'
            e2 = self.req_e and self.req_e == 'E2'
            if self.is_empty(value) and self.required and (e1 or e2):
                msg = "required"
                return not e1, not e2, msg
        
        if self.is_empty(value):
            self.update_text_len_watermarks(value)
            return True, True, None
        
        if strictness >= Field.MODERATE:
            str_to_convert = value
                    
            e1 = self.number_format_e and self.number_format_e == 'E1'
            e2 = self.number_format_e and self.number_format_e == 'E2'
            if self.number_regexp is not None:
                match = re.match(self.number_regexp, value)
                if match is None and (e1 or e2):
                    msg = "no match: value: %s, pattern: %s" % (value, self.number_format)
                    return not e1, not e2, msg
                else:
                    str_to_convert = match.group()
            num = None        
            try:
                if str_to_convert.find('.') >= 0:
                    num = float(str_to_convert)
                else:
                    num = int(str_to_convert)
            except ValueError:
                if e1 or e2:
                    msg = "number cannot be parsed: '%s'" % value
                    return not e1, not e2, msg
        
            if strictness >= Field.STRICT:        
                e1 = self.min_number_e and self.min_number_e == 'E1'
                e2 = self.min_number_e and self.min_number_e == 'E2'
                if self.min_number is not None and num < self.min_number and (e1 or e2):
                    msg = "number < min: value: %s, min: %s" % (value, self.min_number)
                    return not e1, not e2, msg
                    
                e1 = self.max_number_e and self.max_number_e == 'E1'
                e2 = self.max_number_e and self.max_number_e == 'E2'
                if self.max_number is not None and num > self.max_number and (e1 or e2):
                    msg = "number > max: value: %s, max: %s" % (value, self.max_number)
                    return not e1, not e2, msg
                    
            self.update_number_watermarks(num)
            
        self.update_text_len_watermarks(value)
        return True, True, None
        
    def render_metadata(self):
        number_format = self.number_format
        if number_format is not None:
            number_format = "'%s'" % number_format
            
        msg_fmt = "Number, required=%s, calc_freq_cnts=%s, number format=%s, min=%s, max=%s"
        s = msg_fmt % (self.required, self.calc_freq_cnts,
                       number_format, self.min_number, self.max_number)
        return [s]
    
    def render_stats(self):
        stats = super(NumberField, self).render_stats()
        stats.append("number_lwm: %s, number_hwm: %s" % (self.number_lwm, self.number_hwm))
        return stats

    def reset(self):
        super(NumberField, self).reset()
        self.number_lwm = None
        self.number_hwm = None
        
    def update_number_watermarks(self, num):
        # update low watermark
        not_none = num is not None and self.number_lwm is not None
        num_lt = not_none and num < self.number_lwm
        if self.number_lwm is None or num_lt:
            self.number_lwm = num
            
        # update high watermark
        not_none = num is not None and self.number_hwm is not None
        num_gt = not_none and num > self.number_hwm
        if self.number_hwm is None or num_gt:
            self.number_hwm = num


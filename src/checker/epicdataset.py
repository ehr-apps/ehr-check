'''
Created on Jun 10, 2012

@author: rschaaf
'''

from checker import CategoryField, DataSet, DateField, NumberField, Table, TextField
import logging

class EpicDataSet(DataSet):
    '''
    classdocs
    '''
    logger = logging.getLogger("EpicDataSet")
    
    EPIC_FILE_PAT = "(?:.*/)*epic(.*)\.esp.\d{8}"
    
    def __init__(self, table_name_pat):
        '''
        Constructor
        '''
        member = Table("epicmem",
                       "Member",
                       TextField("Patient ID", True, None, True, None, 128),
                       TextField("Medical Record #", True, None, True, None, 25),
                       TextField("Patient Last Name", False, None, True, None, 200),
                       TextField("Patient First Name", False, None, True, None, 200),
                       TextField("Patient Middle Initial", False, None, True, None, 200),
                       TextField("Address Line 1", False, None, True, None, 200),
                       TextField("Address Line 2", False, None, True, None, 100),
                       TextField("City", False, None, True, None, 50),
                       TextField("State", False, None, True, None, 20),
                       TextField("Zip Code", False, None, True, None, 20),
                       TextField("Country", False, None, True, None, 20),
                       TextField("Home Phone Area Code", False, None, True, None, 50),
                       TextField("Home Phone Number", False, None, True, None, 100),
                       TextField("Extension", False, None, True, None, 50),
                       DateField("Date of Birth", False, True, "19000101",
                                 DataSet.relative_date(0)),
                       CategoryField("Gender", False),
                       CategoryField("Race", False),
                       CategoryField("Home Language", False),
                       TextField("Social Security Number", False, None, True, None, 20),
                       TextField("Current Primary Care Physician", False, None, True, None, 128),
                       CategoryField("Marital Status", False),
                       CategoryField("Religion", False),
                       TextField("Aliases", False),
                       TextField("Mother Medical Record #", False, None, True, None, 20),
                       DateField("Death Datetime", False, True, "19600101",
                                  DataSet.relative_date(0)),
                       CategoryField("Center ID", False),
                       CategoryField("Ethnicity", False))
        
        visit = Table("epicvis",
                      "Visit",
                      TextField("Patient ID", True, None, True, None, 128),
                      TextField("Medical Record #", True, None, True, None, 50),
                      TextField("Contact Serial Number", False),
                      DateField("Contact Date", False, True, "19600101",
                                 DataSet.relative_date(0)),
                      CategoryField("Visit Closed Flag", False),
                      DateField("Encounter Closed Date", False, True, "19600101",
                                DataSet.relative_date(0)),
                      TextField("Appointment Staff", False, None, True, None, 128),
                      TextField("Visit Department", False, None, True, None, 30),
                      TextField("Visit Department Name", False, None, True, None, 100),
                      CategoryField("Contact Type", False),
                      DateField("Expected Date of Confinement", False, True, "19600101",
                                DataSet.relative_date(270)), # 270 days from today
                      NumberField("Temperature", False, NumberField.FLOAT),
                      TextField("Level of Service/True CPT Code", False),
                      TextField("Weight", False,
                                "(?P<lbs>\d+(\.\d*)?)\s*lbs?\s*((?P<oz>\d+(\.\d*)?)\s*(o|oz)?)?",
                                True, None, 100),
                      TextField("Height", False,
                                "(?P<feet>\d+(\.\d*)?)\s*'\s*(?P<inches>\d+\.?\d*)?",
                                True, None, 100),
                      NumberField("BP Systolic", False, NumberField.FLOAT),
                      NumberField("BP Diastolic", False, NumberField.FLOAT),
                      NumberField("02 Sat", False, NumberField.FLOAT),
                      NumberField("Peak Flow", False, NumberField.FLOAT),
                      TextField("Diagnosis Code", False),
                      NumberField("BMI", False, NumberField.FLOAT),
                      DateField("Hospital Admission Date", False, True, "19600101",
                                 DataSet.relative_date(0)),
                      DateField("Hospital Discharge Date", False, True, "19600101",
                                 DataSet.relative_date(0)))

        social_history = Table("epicsoc",
                               "Social History",
                               TextField("Patient ID", True, None, True, None, 128),
                               TextField("Medical Record #", True, None, True, None, 50),
                               CategoryField("Tobacco Use", False),
                               CategoryField("Alcohol Use", False),
                               DateField("Date Entered", False, True, "19600101",
                                         DataSet.relative_date(0)),
                               TextField("Social History ID", False),
                               TextField("Provider", False, None, True, None, 128))
                    
        immunizations = Table("epicimm",
                              "Immunization",
                              TextField("Patient ID", True, None, True, None, 128),
                              CategoryField("Type of Immunization", False),
                              CategoryField("Type of Immunization/Immunization Name", False),
                              DateField("Immunization Date Given", False, True, "19600101",
                                        DataSet.relative_date(0)),
                              TextField("Immunization Dose", False, None, True, None, 100),
                              CategoryField("Manufacturer", False),
                              TextField("Lot Number", False),
                              TextField("Immunization Record ID", False, None, True, None, 128),
                              TextField("Medical Record #", True, None, True, None, 50),
                              TextField("Provider", False, None, True, None, 128),
                              DateField("Encounter Date", False, True, "19600101",
                                         DataSet.relative_date(0)),
                              TextField("Immunization Status", False, None, True, None),
                              CategoryField("CPT Code", False),
                              CategoryField("Patient Class", False),
                              CategoryField("Patient Status", False))

        allergies = Table("epicall",
                          "Allergy",
                          TextField("Patient ID", True, None, True, None, 128),
                          TextField("Medical Record #", True, None, True, None, 50),
                          TextField("Problem ID", False, None, True, None, 128),
                          DateField("Date Noted", False, True, "19600101",
                                    DataSet.relative_date(0)),
                          TextField("Allergy ID", False, None, True, None, 100),
                          TextField("Allergy Name", False, None, True, None, 300),
                          CategoryField("Allergy Status", False),
                          TextField("Allergy Description", False, None, True, None, 200),
                          DateField("Allergy Entered Date", False, True, "19600101",
                                     DataSet.relative_date(0)),
                          TextField("Provider", False, None, True, None, 128))
                          
        problem_list = Table("epicprb",
                             "Problem List",
                             TextField("Patient ID", True, None, True, None, 128),
                             TextField("Medical Record #", True, None, True, None, 50),
                             TextField("Problem ID", False, None, True, None, 128),
                             DateField("Date Noted", False, True, "19600101",
                                       DataSet.relative_date(0)),
                             CategoryField("ICD9 Code", False),
                             CategoryField("Problem Status", False),
                             TextField("Comment", False),
                             TextField("Provider", False, None, True, None, 128))

        hosp_problem_list = Table("epichpr",
                             "Hospital Problem List",
                             TextField("Patient ID", True, None, True, None, 128),
                             TextField("Medical Record #", True, None, True, None, 50),
                             TextField("Problem ID", False, None, True, None, 128),
                             DateField("Date Noted", False, True, "19600101",
                                       DataSet.relative_date(0)),
                             CategoryField("ICD9 Code", False),
                             CategoryField("Problem Status", False),
                             CategoryField("Principal Hospital Problem", False),
                             CategoryField("Present On Admission", False),
                             CategoryField("Priority", False),
                             TextField("Overview", False),
                             TextField("Provider", False, None, True, None, 128))

        provider = Table("epicpro",
                         "Provider",
                         TextField("Provider ID", True, None, True, None, 128),
                         TextField("Clinician Last Name", False, None, True, None, 200),
                         TextField("Clinician First Name", False, None, True, None, 200),
                         TextField("Clinician Middle Initial", False, None, True, None, 200),
                         TextField("Clinician Title", False, None, True, None, 20),
                         TextField("Primary Department ID", False, None, True, None, 20),
                         CategoryField("Primary Department", False),
                         TextField("Primary Department Address Line 1", False, None, True, None, 100),
                         TextField("Primary Department Address Line 2", False, None, True, None, 20),
                         TextField("Primary Department City", False, None, True, None, 20),
                         TextField("Primary Department State", False, None, True, None, 20),
                         TextField("Primary Department Zip", False, None, True, None, 20),
                         TextField("Primary Department Phone Area Code", False, None, True, None, 50),
                         TextField("Primary Department Phone", False, None, True, None, 50),
                         CategoryField("Center ID", False))

        orders = Table("epicord",
                       "Lab Order",
                       TextField("Patient ID", True, None, True, None, 128),
                       TextField("Medical Record #", True, None, True, None, 50),
                       TextField("Order ID", False, None, True, None, 128),
                       TextField("Procedure Master #", False, None, True, None, 20),
                       TextField("Modifier", False, None, True, None, 20),
                       TextField("Specimen ID", False, None, True, None, 30),
                       DateField("Ordering Date", False, True, "19600101",
                                 DataSet.relative_date(0)),
                       CategoryField("Order Type", False),
                       TextField("Ordering Provider", False, None, True, None, 128),
                       CategoryField("Procedure Name", False),
                       CategoryField("Specimen Source", False),
                       CategoryField("Test Status", False),
                       CategoryField("Patient Class", False),
                       CategoryField("Patient Status", False))
                       
        results = Table("epicres",
                        "Lab Result",
                        TextField("Patient ID", True, None, True, None, 128),
                        TextField("Medical Record #", True, None, True, None, 50),
                        TextField("Order ID", False, None, True, None, 128),
                        DateField("Ordering Date", False, True, "19600101",
                                  DataSet.relative_date(0)),
                        DateField("Result Date", False, True, "19600101",
                                  DataSet.relative_date(0)),
                        TextField("Ordering Provider", False, None, True, None, 128),
                        CategoryField("Order Type", False),
                        TextField("Procedure Master #", False, None, True, None, 255),
                        CategoryField("Test Code", False),
                        CategoryField("Test Name", False),
                        TextField("Test Results", False),
                        CategoryField("Normal Flag", False),
                        TextField("Reference Low", False, None, True, None, 100),
                        TextField("Reference High", False, None, True, None, 100),
                        TextField("Reference Unit", False, None, True, None, 100),
                        CategoryField("Result Status", False),
                        TextField("Comment Result", False),
                        TextField("Specimen ID", False, None, True, None, 100),
                        TextField("Impression", False),
                        TextField("Specimen Source", False, None, True, None, 255),
                        DateField("Collection Date", False, True, "19600101",
                                  DataSet.relative_date(0)),
                        CategoryField("Procedure Name", False),
                        TextField("Lab Result ID", False, None, True, None, 128),
                        CategoryField("Patient Class", False),
                        CategoryField("Patient Status", False))
                       
        medications = Table("epicmed",
                            "Medication",
                            TextField("Patient ID", True, None, True, None, 128),
                            TextField("Medical Record #", True, None, True, None, 50),
                            TextField("Order ID", False, None, True, None, 128),
                            TextField("Prescribing Provider", False, None, True, None, 128),
                            DateField("Ordering Date", False, True, "19600101",
                                      DataSet.relative_date(0)),
                            CategoryField("Order Status", False),
                            TextField("Meds - Directions", False),
                            CategoryField("NDC Code", False),
                            TextField("Original Rx - Medication Name", False),
                            TextField("Original Rx - Quantity", False, None, True, None, 200),
                            TextField("Original Rx - Refills", False, None, True, None, 200),
                            DateField("Original Rx - Start Date", False, True, "19600101",
                                      DataSet.relative_date(0)),
                            DateField("Original Rx - End Date", False, True, "19600101",
                                      DataSet.relative_date(10 * 365)), # 10 years from now
                            CategoryField("Route", False),
                            TextField("Dose", False, None, True, None, 200),
                            CategoryField("Patient Class", False),
                            CategoryField("Patient Status", False))

        pregnancy_info = Table("epicprg",
                               "Pregnancy Info",
                               TextField("Patient ID", True, None, True, None, 128),
                               TextField("Medical Record #", True, None, True, None, 50),
                               TextField("Provider ID", False, None, True, None, 128),
                               TextField("Pregnancy ID", False),
                               CategoryField("Outcome", False),
                               DateField("Estimated Date of Delivery", False, True, "19600101",
                                         DataSet.relative_date(270)), # 270 days from now
                               DateField("Actual Date of Delivery", False, True, "19600101",
                                         DataSet.relative_date(0)),
                               NumberField("Gravida", False, NumberField.POSINT),
                               NumberField("Parity", False, NumberField.POSINT),
                               NumberField("Term", False, NumberField.POSINT),
                               NumberField("Preterm", False, NumberField.POSINT),
                               TextField("Gestational Age at Delivery", False,
                                         "(?P<w>\d+(\.\d*)?)\s*w\s*((?P<d>\d+(\.\d*)?)\s*d?)?", True, None, 20),
                               TextField("Birth Weight", False,
                                         "(?P<lbs>\d+(\.\d*)?)\s*lbs?\s*((?P<oz>\d+(\.\d*)?)\s*(o|oz)?)?",
                                         True, None, 200),
                               CategoryField("Delivery", False),
                               TextField("Pre-eclampsia", False, None, True, None, 300))

        super(EpicDataSet, self).__init__("ESP Epic load files", "20120606 ETL spec",
                                          self.DATASET_EPIC, table_name_pat,
                                          member, visit, social_history, immunizations,
                                          allergies, problem_list, hosp_problem_list,
                                          provider, orders, results, medications,
                                          pregnancy_info)
    
    def read_table(self, table_name):
        self.logger.debug("reading from table '%s'" % table_name)
        f = open(table_name)
        line = f.readline()
        while line:
            yield line.rstrip("\r\n").split("^")
            line = f.readline()
        f.close()
        return


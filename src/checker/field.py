'''
Created on Jun 5, 2012

@author: rschaaf
'''

class Field(object):
    '''
    Base class for field data checks and stats
    '''
    
    # Levels of data checking strictness
    LOOSE = 0
    MODERATE = 1
    STRICT = 2

    def __init__(self, name, required=False, calc_freq_cnts=True):
        '''
        Constructor
        '''
        self.name = name
        self.required = required
        self.calc_freq_cnts = calc_freq_cnts
        self.reset()

    def merge(self, field):
        self.valid_cnt += field.valid_cnt
        self.error_cnt += field.error_cnt
        self.e2error_cnt += field.e2error_cnt
        self.empty_cnt += field.empty_cnt
        self.text_len_lwm = self.min_(self.text_len_lwm, field.text_len_lwm)
        self.text_len_hwm = self.max_(self.text_len_hwm, field.text_len_hwm)
        for key in field.freq_cnts.keys():
            if key in self.freq_cnts:
                self.freq_cnts[key] += field.freq_cnts[key]
            else:
                self.freq_cnts[key] = field.freq_cnts[key]
        
        for key in field.errors.keys():
            if key in self.errors:
                self.errors[key] += field.errors[key]
            else:
                self.errors[key] = field.errors[key]

        for key in field.e2errors.keys():
            if key in self.e2errors:
                self.e2errors[key] += field.e2errors[key]
            else:
                self.e2errors[key] = field.e2errors[key]
    
    def render_metadata(self):
        s = "Field, required=%s, calc_freq_cnts=%s" % (self.required, self.calc_freq_cnts)
        return [s]
    
    def render_stats(self):
        s = "valid: %d, empty: %d, e1errors: %d, e2errors: %d, " % (self.valid_cnt,
                                                    self.empty_cnt,
                                                    self.error_cnt,
                                                    self.e2error_cnt)
        s += "len_lwm: %s, len_hwm: %s" % (self.text_len_lwm,
                                           self.text_len_hwm)
        return [s]

    def render_freq_cnts(self):
        freq_cnts = []
        keys = self.freq_cnts.keys()
        sorted_keys = sorted(keys)
        for k in sorted_keys:
            freq_cnts.append("%8d %s" % (self.freq_cnts[k], k))
        return freq_cnts

    def render_errors(self):
        keys = self.errors.keys()
        sorted_keys = sorted(keys)
        err_strings = []
        for k in sorted_keys:
            err_strings.append("%s: %d" % (k, self.errors[k]))
        return err_strings

    def render_e2errors(self):
        keys = self.e2errors.keys()
        sorted_keys = sorted(keys)
        err_strings = []
        for k in sorted_keys:
            err_strings.append("%s: %d" % (k, self.e2errors[k]))
        return err_strings
            
    def reset(self):
        self.valid_cnt = 0
        self.error_cnt = 0
        self.e2error_cnt = 0
        self.empty_cnt = 0
        self.text_len_lwm = None
        self.text_len_hwm = None
        self.freq_cnts = {}
        self.errors = {}
        self.e2errors = {}
    
    def is_valid(self, value, strictness=STRICT):
        # If the strictness level is MODERATE or STRICT, then flag as
        # errors empty values for required fields
        if strictness >= self.MODERATE:
            if self.required and self.is_empty(value):
                msg = "required"
                return False, True, msg
            
        self.update_text_len_watermarks(value)
        return True, True, None
    
    def tally_valid(self, value):
        self.valid_cnt += 1
        if self.calc_freq_cnts:
            if value in self.freq_cnts:
                self.freq_cnts[value] += 1
            else:
                self.freq_cnts[value] = 1
        
        if self.is_empty(value):
            self.empty_cnt += 1
    
    def tally_error(self, msg):
        self.error_cnt += 1
        if msg in self.errors:
            self.errors[msg] += 1
        else:
            self.errors[msg] = 1
    
    def tally_e2error(self, msg):
        self.e2error_cnt += 1
        if msg in self.e2errors:
            self.e2errors[msg] += 1
        else:
            self.e2errors[msg] = 1

    def is_empty(self, value):
        return value is None or value.strip() == ""
    
    def update_text_len_watermarks(self, value):
        if value is None:
            text_len = 0
        else:
            text_len = len(value)
        
        # update low watermark
        if self.text_len_lwm is None or text_len < self.text_len_lwm:
            self.text_len_lwm = text_len
            
        # update high watermark
        if self.text_len_hwm is None or text_len > self.text_len_hwm:
            self.text_len_hwm = text_len
    
    @staticmethod
    def min_(a, b):
        if a is None:
            return b
        if b is None:
            return a
        return min(a, b)
    
    @staticmethod
    def max_(a, b):
        if a is None:
            return b
        if b is None:
            return a
        return max(a, b)

'''
Created on Jun 5, 2012

@author: rschaaf
'''

from checker import Field
import json
import os.path

class CategoryField(Field):
    '''
    Category field data checks and stats
    '''

    def __init__(self, name, required=False, category_file=None, allow_adds=True, calc_freq_cnts=True):
        '''
        Constructor
        '''
        self.category_file = category_file
        self.allow_adds = allow_adds
        
        # must be called after assigning a value to category_file instance variable
        super(CategoryField, self).__init__(name, required, calc_freq_cnts)
        
    def merge(self, field):
        super(CategoryField, self).merge(field)
        for key in field.categories.keys():
            if key in self.categories:
                self.categories[key] += field.categories[key]
            else:
                self.categories[key] = field.categories[key]

        
    def is_valid(self, value, strictness=Field.STRICT):
        if strictness >= Field.MODERATE:
            if self.required and self.is_empty(value):
                return False, "required"
        
        if self.is_empty(value):
            self.update_text_len_watermarks(value)
            return True, None
        
        valid = None
        msg = None
        if value in self.categories:
            valid = True
            self.categories[value] += 1
        elif self.allow_adds or strictness < Field.STRICT:
            valid = True
            self.categories[value] = 1
        else:
            valid = False
            msg = "not in vocabulary: value = '%s'" % value
            
        if valid:
            self.update_text_len_watermarks(value)
            
        return valid, msg
        
    def render_metadata(self):
        fmt = "Category, required=%s, calc_freq_cnts=%s, file=%s, allow_adds=%s"
        s = fmt % (self.required, self.calc_freq_cnts, self.category_file, self.allow_adds)
        return [s]
    
    def render_stats(self):            
        stats = super(CategoryField, self).render_stats()
        stats.append("category_cnt: %d" % len(self.categories))
        
        keys = self.categories.keys()
        keys.sort()
        for k in keys:
            stats.append(k)
            #stats.append("%8d %s" % (self.categories[k], k))
            
        return stats

    def reset(self):
        super(CategoryField, self).reset()
        self.load_category_stats()
        
    def load_category_stats(self):
        self.categories = {}
        if self.category_file is not None:
            if os.path.exists(self.category_file):
                f = open(self.category_file, "r")
                try:
                    self.categories = json.load(f)
                finally:
                    f.close()
     
    def store_category_stats(self):
        if self.category_file is not None:
            f = open(self.category_file, "w")
            try:
                json.dump(self.categories, f, sort_keys=True)
            finally:
                f.close()
        
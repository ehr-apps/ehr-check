'''
Created on Jun 17, 2012

@author: rschaaf
'''

from checker import CategoryField, DataSet, DateField, NumberField, Table, TextField
import datetime
import logging

class PgDataSet(DataSet):
    '''
    classdocs
    '''
    logger = logging.getLogger("PgDataSet")
    FETCH_SIZE = 10000
    POSTGRES_PAT = "(.*)"
    
    def __init__(self, db_conn, description, schema, table_patterns):
        '''
        Constructor
        '''
        self.db_conn = db_conn
        self.schema = schema
        table_names = self.find_matching_tables(db_conn, schema, table_patterns)
        
        tables = []
        for name in table_names:
           tables.append(self.get_table_metadata(db_conn, schema, name))
        
        super(PgDataSet, self).__init__("PostgreSQL tables", description,
                                        self.DATASET_POSTGRES, self.POSTGRES_PAT,
                                        *tables)

    def row_to_strings(self, row):
        strs = []
        date_fmt = "%02d%02d%02d"
        datetime_fmt = "%02d%02d%02d %02d:%02d:%02d"
        for item in row:
            if item is None:
                strs.append("")
            elif isinstance(item, datetime.date):
                strs.append(date_fmt % (item.year, item.month, item.day))
                # The following doesn't work for dates before 1900
                # strs.append(item.strftime("%Y%m%d"))
            elif isinstance(item, datetime.datetime):
                strs.append(datetime_fmt % (item.year, item.month, item.day,
                                            item.hour, item.minute, item.second))
                # The following doesn't work for dates before 1900
                # strs.append(item.strftime("%Y%m%d%H%M%S"))
            else:
                strs.append("%s" % item)
        return strs
    
    def read_table(self, table_name):
        self.logger.debug("reading from table '%s'" % table_name)
        cursor = self.db_conn.cursor()
        cursor.execute("select * from %s.%s" % (self.schema, table_name))
        rows = cursor.fetchmany(self.FETCH_SIZE)
        while len(rows) > 0:
            for row in rows:
                yield self.row_to_strings(row)
            rows = cursor.fetchmany(self.FETCH_SIZE)           
        cursor.close()
        return
    
    def find_matching_tables(self, conn, schema, table_patterns):
        tables = []
        try:
            cursor = conn.cursor()
            for table_pattern in table_patterns:
                cursor.execute("select table_name from information_schema.tables" +
                               " where table_schema = %(schema)s and table_name like %(pat)s " +
                               "order by table_name",
                               { 'schema': schema,
                                 'pat': table_pattern })
                for row in cursor.fetchall():
                    tables.append(row[0])
        except StandardError as e:
            self.logger.exception("Database access error:")
        finally:
            cursor.close()
            
        return tables
    
    def get_table_metadata(self, conn, schema, name):
        fields = [];
        try:
            cursor = conn.cursor()
            cursor.execute("select ordinal_position, column_name, data_type," +
                           "       is_nullable, character_maximum_length" +
                           "  from information_schema.columns" +
                           " where table_schema = %(schema)s and table_name = %(name)s " +
                           "order by ordinal_position",
                           { "schema": schema, "name": name })
            for row in cursor.fetchall():
                fields.append(self.get_field_metadata(row[1], row[2], row[3], row[4]))               
        except StandardError as e:
            self.logger.exception("Database access error:")
        finally:
            cursor.close()
                    
        return Table(name, name, *fields)
    
    def get_field_metadata(self, name, data_type, nullable, max_length):
        name = name.lower()
        is_id = (name == "id")
        data_type = data_type.lower()
        required = (nullable == "NO")
        
        if data_type.startswith("character") or data_type.startswith("text"):
            return TextField(name, required, None, True, None, max_length)
        elif data_type.startswith("boolean"):
            return CategoryField(name, required)
        elif data_type.startswith("date") or data_type.startswith("timestamp"):
            return DateField(name, required)
        elif data_type.find("int") >= 0:
            return NumberField(name, required, NumberField.INT, not is_id)
        elif data_type.startswith("double precision"):
            return NumberField(name, required, NumberField.FLOAT)
        else:
            self.logger.error("Unexpected data type '%s'" % data_type)
            return None


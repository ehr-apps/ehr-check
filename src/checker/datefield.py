'''
@author: Jeff Andre
@date: March 29, 2021
'''

from checker import Field
import datetime
import re

class DateField(Field):
    '''
    Date field data checks and stats
    '''
    ESP_DATETIME_TZ = { "regexp_pattern": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}-\d{2}:\d{2}",
                          "date_format": "%Y-%m-%dT%H:%M:%S%z",
                          "match_length": 25 }
    ESP_DATETIME_SEC = { "regexp_pattern": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}",
                          "date_format": "%Y-%m-%dT%H:%M:%S",
                          "match_length": 19 }
    ESP_DATETIME_MIN = { "regexp_pattern": "\d{4}-\d{2}-\d{2}T\d{2}:\d{2}",
                          "date_format": "%Y-%m-%dT%H:%M",
                          "match_length": 16 }
    ESP_DATE_YYYY_MM_DD = { "regexp_pattern": "\d{4}-\d{2}-\d{2}",
                          "date_format": "%Y-%m-%d",
                          "match_length": 10 }
    
    ESP_DATE_FORMAT_BY_LEN = {25: ESP_DATETIME_TZ,
                              19: ESP_DATETIME_SEC,
                              16: ESP_DATETIME_MIN,
                              10: ESP_DATE_YYYY_MM_DD}

    ESP_DATETIME_PATTERN = "(%s)|(%s)|(%s)|(%s)" % (ESP_DATETIME_TZ["regexp_pattern"],
                                                    ESP_DATETIME_SEC["regexp_pattern"],
                                                    ESP_DATETIME_MIN["regexp_pattern"],
                                                    ESP_DATE_YYYY_MM_DD["regexp_pattern"])
    
    
    DEFAULT_DATE_FMT = ESP_DATE_YYYY_MM_DD["date_format"]
    
    
    def __init__(self, name, required, calc_freq_cnts=True, 
                 min_date=None, max_date=None, unique=None,
                 req_e=None, min_date_e=None, max_date_e=None, unique_e=None):
        '''
        Constructor
        '''
        super(DateField, self).__init__(name, required, calc_freq_cnts)
        self.min_date = None
        self.max_date = None
        self.date_lwm = None
        self.date_hwm = None
        if min_date is not None:
            parse_fmt = self._get_date_parse_format(min_date)
            self.min_date = datetime.datetime.strptime(min_date, parse_fmt)
        if max_date is not None:
            parse_fmt = self._get_date_parse_format(max_date)
            self.max_date = datetime.datetime.strptime(max_date, parse_fmt)

        self.req_e = req_e
        self.min_date_e = min_date_e
        self.max_date_e = max_date_e
        self.unique = unique
        self.unique_e = unique_e

    def merge(self, field):
        super(DateField, self).merge(field)
        self.date_lwm = self.min_(self.date_lwm, field.date_lwm)
        self.date_hwm = self.max_(self.date_hwm, field.date_hwm)
        
    def is_valid(self, value, strictness=Field.STRICT):
        if strictness >= Field.MODERATE:
            e1 = self.req_e and self.req_e == 'E1'
            e2 = self.req_e and self.req_e == 'E2'
            if self.is_empty(value) and self.required and (e1 or e2):
                msg = "required"
                return not e1, not e2, msg
        
        if self.is_empty(value):
            self.update_text_len_watermarks(value)
            return True, True, None

        if strictness >= Field.MODERATE:        
            try:
                match = re.match(self.ESP_DATETIME_PATTERN, value)
                if match is None:
                    raise ValueError()
                
                if strictness >= Field.STRICT:
                    match_str = match.group()
                    if match_str != value:
                        msg = "partial match of '%s' within '%s'" % (match_str, value)
                        return False, True, msg
                parse_fmt = self._get_date_parse_format(value)
                # remove colon in tz to parse time with strptime (python 3.6 issue)
                v = value[:-3] + value[-3:].replace(":", "") if len(value) == 25 else value
                # make date naive after parsing for following checks (min, max, etc)
                date = datetime.datetime.strptime(v, parse_fmt).replace(tzinfo=None)
            except ValueError:
                msg = "date cannot be parsed: '%s'" % value
                return False, True, msg
            
        if strictness >= Field.STRICT:
            e1 = self.min_date_e and self.min_date_e == 'E1'
            e2 = self.min_date_e and self.min_date_e == 'E2'
            e1e2 = e1 or e2
            if self.min_date is not None and date < self.min_date and e1e2:
                min_dt = self.min_date.date().isoformat()
                msg = "date < minimum date: date: %s, mindate: %s" % (value, min_dt)
                return not e1, not e2, msg
            
            if self.max_date is not None and date > self.max_date and e1e2:
                e1 = self.max_date_e and self.max_date_e == 'E1'
                e2 = self.max_date_e and self.max_date_e == 'E2'
                max_dt = self.max_date.date().isoformat()
                msg = "date > maximum date: date: %s, maxdate: %s" % (value, max_dt)
                return not e1, not e2, msg
        
        self.update_text_len_watermarks(value)
        
        if strictness >= Field.MODERATE:        
            self.update_date_watermarks(date)
            
        return True, True, None
        
    def render_metadata(self):
        fmt = "Date, required=%s, calc_freq_cnts=%s, min date=%s, max date=%s"
        min_date = self.min_date
        max_date = self.max_date
        
        if min_date is not None:
            min_date = min_date.date().isoformat()
            
        if max_date is not None:
            max_date = max_date.date().isoformat()
            
        s = fmt % (self.required, self.calc_freq_cnts, min_date, max_date)
        return [s]
    
    def render_stats(self):
        date_format = self.DEFAULT_DATE_FMT
        date_lwm = self.date_lwm
        if date_lwm is not None:
            date_lwm = date_lwm.date().isoformat()
        
        date_hwm = self.date_hwm
        if date_hwm is not None:
            date_hwm = date_hwm.date(). isoformat()
            
        stats = super(DateField, self).render_stats()
        stats.append("date_lwm: %s, date_hwm: %s" % (date_lwm, date_hwm))
        return stats

    def reset(self):
        super(DateField, self).reset()
        self.date_lwm = None
        self.date_hwm = None
        
    def update_date_watermarks(self, date):
        if date is None:
            return
        
        self.date_lwm = Field.min_(self.date_lwm, date)
        self.date_hwm = Field.max_(self.date_hwm, date)
            
    def _get_date_parse_format(self, value):
        dict = self.ESP_DATE_FORMAT_BY_LEN[len(value.rstrip())]
        return dict["date_format"]
        
        
        
        

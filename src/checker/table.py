'''
@author: Jeff Andre <jandre@commoninf.com>
@date: March 29, 2021
'''

from checker import CategoryField, Field

class Table(object):
    '''
    classdocs
    '''
    INDENT = "  "
    
    def __init__(self, name, description, *fields):
        '''
        Constructor
        '''
        self.name = name
        self.description = description
        self.fields = []
        for field in fields:
            self.fields.append(field)
        self.reset_row_stats()
        
    def process_row(self, row, check_level=Field.STRICT):
        self.row_num += 1
        err_count = 0
        e2err_count = 0
        row_dict = {}
        msgs = []
        e2_msgs = []
        field_index = 0
        unique_key = ''
        unique_e1 = False
        unique_e2 = False
        for field in self.fields:
            if field_index < len(row):
                item = row[field_index]
                row_dict[field.name] = item
            else:
                item = ""
                
            e1valid, e2valid, msg = field.is_valid(item, check_level)
            if e1valid and e2valid:
                field.tally_valid(item)
                if field.unique:
                    if item: unique_key += item +'|'
                    if field.unique_e == 'E1': unique_e1 = True
                    if field.unique_e == 'E2': unique_e2 = True
            elif not e1valid:
                err_count += 1
                field.tally_error(msg)
                msgs.append("row %d, E1, '%s': %s" % (self.row_num, field.name, msg))
            elif not e2valid:
                e2err_count += 1
                field.tally_e2error(msg)
                e2_msgs.append("row %d, E2, '%s': %s" % (self.row_num, field.name, msg))
                
            field_index += 1

        unique_e = False
        if unique_e1:
            unique_e = 'E1'
        elif unique_e2:
            unique_e = 'E2'

        num_fields = len(self.fields)
        if len(row) > num_fields:
            err_count += 1
            msgs.append("row %d: Extra row data: %s" % (self.row_num, row[num_fields:]))
        e1 = err_count > 0
        e2 = e2err_count > 0
        if not e1 and not e2:
            self.valid_rows += 1
        else:
            if e1: self.error_rows +=1
            if e2: self.e2error_rows +=1
        return not e1, not e2, msgs, e2_msgs, unique_e, unique_key
    
    def reset_field_stats(self):
        for field in self.fields:
            field.reset()

    def reset_row_stats(self):
        self.row_num = 0
        self.valid_rows = 0
        self.error_rows = 0
        self.e2error_rows = 0
        self.e1_duplicates = 0
        self.e2_duplicates = 0
        
    def reset(self):
        self.reset_field_stats()
        self.reset_row_stats()

    def merge(self, table):
        self.row_num += table.row_num
        self.valid_rows += table.valid_rows
        self.error_rows += table.error_rows
        self.e2error_rows += table.e2error_rows
        
        for index in range(0, len(table.fields)):
            self.fields[index].merge(table.fields[index])
    
    def update_stored_counts(self):
        for field in self.fields:
            if isinstance(field, CategoryField):
                field.store_category_stats()
            
    def render_metadata(self, include_field_metadata=True, verbose=False):
        metadata = [ "File '%s'" % (self.name) ]
        if include_field_metadata:
            field_pos = 1
            for field in self.fields:
                if verbose or field.error_cnt > 0 or field.e2_error_cnt > 0:
                    field_metadata = field.render_metadata()
                    metadata.append("%s%d: Field '%s', %s" % (self.INDENT, field_pos,
                                                              field.name,
                                                              field_metadata[0]))
                    for line in field_metadata[1:]:
                        metadata.append("%s%s" % (self.INDENT, line))
                field_pos += 1
        return metadata
    
    def render_results(self, include_field_info=True, include_field_errors=True,
                       include_field_e2errors=True, include_freq_cnts=True, verbose=False):
        results = []
        tot_rows = self.valid_rows + self.error_rows + self.e2error_rows
        
        if (verbose and tot_rows > 0) or self.error_rows > 0 or self.e2error_rows > 0:
            results = self.render_metadata(False, verbose)
            if self.error_rows > 0 or self.e2error_rows > 0:
                results.append("")
                results.append("There are data errors to address.  Details are available in error.log.")
                results.append("")
                return results
            fmt = "%svalid_rows=%s, E1 error_rows=%s, E2 error_rows=%s"
            results.append(fmt % (self.INDENT, self.valid_rows, 
                                self.error_rows, self.e2error_rows))
            fmt = "%sE1 duplicates=%s, E2 duplicates=%s"
            results.append(fmt % (self.INDENT,  self.e1_duplicates, self.e2_duplicates))
    
            if include_field_info:
                field_pos = 1
                for field in self.fields:
                    if verbose or field.error_cnt > 0 or field.e2error_cnt > 0:
                        results.append("")   # add an empty line for readability
                        results.append("%s%d: Field '%s'" % (self.INDENT, field_pos, field.name))
                        
                        field_metadata = field.render_metadata()
                        for line in field_metadata:
                            results.append("%s%s%s" % (self.INDENT, self.INDENT, line))
        
                        field_stats = field.render_stats()
                        for line in field_stats:
                            results.append("%s%s%s" % (self.INDENT, self.INDENT, line))
                            
                        if include_field_errors:
                            results.append("")
                            if field.error_cnt == 0:
                                results.append("%sNo E1 errors" % self.INDENT)
                            else:
                                results.append("%sE1 errors" % self.INDENT)
                                errors = field.render_errors()
                                for err_line in errors:
                                    results.append("%s%s%s" % (self.INDENT, self.INDENT, err_line))
                                    
                        if include_field_e2errors:
                            results.append("")
                            if field.e2error_cnt == 0:
                                results.append("%sNo E2 errors" % self.INDENT)
                            else:
                                results.append("%sE2 errors" % self.INDENT)
                                errors = field.render_e2errors()
                                for err_line in errors:
                                    results.append("%s%s%s" % (self.INDENT, self.INDENT, err_line))
                                    
                        if include_freq_cnts and field.calc_freq_cnts:
                            results.append("")
                            results.append("%sFrequency counts (num distinct=%d)" % (self.INDENT,
                                                                                     len(field.freq_cnts)))
                            freq_cnts = field.render_freq_cnts()
                            for line in freq_cnts:
                                results.append("%s%s%s" % (self.INDENT, self.INDENT, line))
                                               
                    field_pos += 1                   
                    
        return results

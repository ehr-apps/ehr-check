'''
Created on Jun 5, 2012

@author: rschaaf
'''
from checker import Field, TextField
import unittest

class TextFieldTest(unittest.TestCase):
    ONES = "1+"
    TWOS = "2+"
    
    def setUp(self):
        self.min_len = 3
        self.max_len = 6
        self.text_alpha_required = TextField("text_alpha_required", True, TextField.ALPHA, False)
        self.text_alpha_not_required = TextField("text_alpha_not_required", False, TextField.ALPHA, False)
        self.text_nopat_required = TextField("text_nopat_required", True, None, False)
        self.text_ones_with_min = TextField("text_ones_with_min", True, self.ONES, False, self.min_len)
        self.text_twos_with_max = TextField("text_twos_with_max", True, self.TWOS, False, None, self.max_len)

    def tearDown(self):
        pass

    def test_constructor(self):
        self.assert_(self.text_alpha_required.required, "required")
        self.assert_(not self.text_alpha_not_required.required, "not required")
        self.assert_(self.text_alpha_required.min_len is None, "no min_len")
        self.assert_(self.text_alpha_required.max_len is None, "no max_len")
        
        self.assertEqual(self.text_ones_with_min.min_len, 3, "min_len specified")
        self.assertEqual(self.text_twos_with_max.max_len, 6, "max_len specified")
        
        self.assert_(self.text_nopat_required.text_format is None, "no text format")
        self.assertEqual(self.text_alpha_required.text_format, TextField.ALPHA, "ALPHA as text format")
        self.assertEqual(self.text_ones_with_min.text_format, self.ONES, "ONES as text format")
        self.assertEqual(self.text_twos_with_max.text_format, self.TWOS, "TWOS as text format")
    
    def test_parse_full_match(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.text_alpha_required.is_valid("NowIsTheTime", strictness)
            self.assert_(valid, "parse a valid text string  (strictness=%d)" % strictness)
        
            valid, msg = self.text_alpha_required.is_valid("1234", strictness)
            assert_msg = "parse an invalid text string (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("no match: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)
                self.assert_(msg is None, assert_msg)
        
            valid, msg = self.text_nopat_required.is_valid("Any chars 123", strictness)
            assert_msg = "parse an invalid text string (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
    def test_parse_partial_match(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            assert_msg = "parse a partially matching text string (strictness=%d)" % strictness
            for str in ["Now ", "Now is"]:
                valid, msg = self.text_alpha_required.is_valid(str, strictness)
                self.assert_(valid, assert_msg)

    def test_required(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.text_nopat_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for required text (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
            else:
                self.assert_(valid, assert_msg)
               
            valid, msg = self.text_nopat_required.is_valid(None, strictness)
            assert_msg = "null value for required text (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
            valid, msg = self.text_alpha_not_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for not required text (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)

            valid, msg = self.text_alpha_not_required.is_valid(None, strictness)
            assert_msg = "null value for not required text (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
    def test_out_of_range(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.text_ones_with_min.is_valid("111", strictness)
            assert_msg = "text is the minimum length (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.text_ones_with_min.is_valid("11", strictness)
            assert_msg = "text less than the minimum length (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("value < minimum length: value: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)

            valid, msg = self.text_twos_with_max.is_valid("222222", strictness)
            assert_msg = "text is the maximum length (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.text_twos_with_max.is_valid("2222222", strictness)
            assert_msg = "text greater than the maximum length (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("value > maximum length: value: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)
    
    def test_merge(self):
        alpha1 = TextField("alpha1", True, TextField.ALPHA, False)
        alpha2 = TextField("alpha2", True, TextField.ALPHA, False)
        valid, msg = alpha1.is_valid("abcd")
        alpha1.merge(alpha2)
        self.assertEqual(alpha1.text_len_lwm, 4, "merge None with not-None value")
        self.assertEqual(alpha1.text_len_hwm, 4, "merge None with not-None value")
        
        alpha1.reset()
        alpha2.reset()
        valid, msg = alpha2.is_valid("abcd")
        alpha1.merge(alpha2)
        self.assertEqual(alpha1.text_len_lwm, 4, "merge not-None with None value")
        self.assertEqual(alpha1.text_len_hwm, 4, "merge not-None with None value")
        
        alpha1.reset()
        alpha2.reset()
        valid, msg = alpha1.is_valid("ab")
        valid, msg = alpha1.is_valid("abcd")
        valid, msg = alpha2.is_valid("abc")
        alpha1.merge(alpha2)
        self.assertEqual(alpha1.text_len_lwm, 2, "merge within-watermark values")
        self.assertEqual(alpha1.text_len_hwm, 4, "merge within-watermark values")
        
        alpha1.reset()
        alpha2.reset()
        valid, msg = alpha1.is_valid("abc")
        valid, msg = alpha2.is_valid("a")
        valid, msg = alpha2.is_valid("abcde")
        alpha1.merge(alpha2)
        self.assertEqual(alpha1.text_len_lwm, 1, "merge outside-watermark values")
        self.assertEqual(alpha1.text_len_hwm, 5, "merge outside-watermark values")

    def test_reset(self):
        assert_msg = "after processing abc"
        valid, msg = self.text_alpha_required.is_valid("abc")
        self.assertEqual(self.text_alpha_required.text_len_lwm, 3, assert_msg)
        self.assertEqual(self.text_alpha_required.text_len_hwm, 3, assert_msg)
        
        self.text_alpha_required.reset()
        
        assert_msg = "after reset"
        self.assert_(self.text_alpha_required.text_len_lwm is None, assert_msg)
        self.assert_(self.text_alpha_required.text_len_hwm is None, assert_msg)

    def test_update_watermarks(self):
        assert_msg = "prior to processing"
        self.assert_(self.text_alpha_not_required.text_len_lwm is None, assert_msg)
        self.assert_(self.text_alpha_not_required.text_len_hwm is None, assert_msg)
        
        assert_msg = "after processing whitespace"
        valid, msg = self.text_alpha_not_required.is_valid("  ")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_lwm, 2, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_hwm, 2, assert_msg)
        
        assert_msg = "after processing null"
        valid, msg = self.text_alpha_not_required.is_valid(None)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_hwm, 2, assert_msg)
        
        assert_msg = "after processing abcdef"
        valid, msg = self.text_alpha_not_required.is_valid("abcdef")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.text_alpha_not_required.text_len_hwm, 6, assert_msg)
    
    def test_render_metadata(self):
        text_alpha_required_meta = self.text_alpha_required.render_metadata()
        text_alpha_not_required_meta = self.text_alpha_not_required.render_metadata()
        text_nopat_required_meta = self.text_nopat_required.render_metadata()
        text_ones_with_min_meta = self.text_ones_with_min.render_metadata()
        text_twos_with_max_meta = self.text_twos_with_max.render_metadata()

        self.assertEqual(text_alpha_required_meta[0],
                         "Text, required=True, calc_freq_cnts=False, " +
                            "text format='[a-zA-Z]+', min len=None, max len=None",
                         "text_alpha_required metadata")
        self.assertEqual(text_alpha_not_required_meta[0],
                         "Text, required=False, calc_freq_cnts=False, " +
                            "text format='[a-zA-Z]+', min len=None, max len=None",
                         "text_alpha_not_required metadata")

        self.assertEqual(text_nopat_required_meta[0],
                         "Text, required=True, calc_freq_cnts=False, " +
                            "text format=None, min len=None, max len=None",
                         "text_nopat_required metadata")

        self.assertEqual(text_ones_with_min_meta[0],
                         "Text, required=True, calc_freq_cnts=False, " +
                            "text format='1+', min len=3, max len=None",
                         "text_ones_with_min metadata")

        self.assertEqual(text_twos_with_max_meta[0],
                         "Text, required=True, calc_freq_cnts=False, " +
                            "text format='2+', min len=None, max len=6",
                         "text_twos_with_max metadata")
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
'''
Created on Jun 8, 2012

@author: rschaaf
'''
from checker import CategoryField, Field
import json
import os.path
import tempfile
import unittest

class CategoryFieldTest(unittest.TestCase):
    
    def setUp(self):
        # Create a .json file for the "yes_no" dictionary
        self.yes_no_path = tempfile.mktemp(".json", "yes_no")
        yes_no_dict = { "yes": 3, "no": 2}
        f = open(self.yes_no_path, "w")
        json.dump(yes_no_dict, f)
        f.close()
        
        # File names to use for the json files dynamically created by the tests
        self.ynu_path = tempfile.mktemp(".json", "ynu")
        if os.path.exists(self.ynu_path):
            os.remove(self.ynu_path)    
        
        self.yes_no_required_allow = CategoryField("yes_no_required_allow",
                                                   True,
                                                   self.yes_no_path,
                                                   True)
        self.yes_no_required_noallow = CategoryField("yes_no_required_noallow",
                                                     True,
                                                     self.yes_no_path,
                                                     False)
        self.ynu_allow = CategoryField("ynu_allow",
                                       False,
                                       self.ynu_path,
                                       True)

    def tearDown(self):
        for path in (self.yes_no_path,
                     self.ynu_path):
            if os.path.exists(path):
                os.remove(path)
  
    def test_constructor(self):
        assert_msg = "yes_no_required_allow"
        self.assertEqual(self.yes_no_required_allow.name, "yes_no_required_allow",
                         assert_msg)
        self.assertEqual(self.yes_no_required_allow.category_file,
                         self.yes_no_path,
                         assert_msg)
        self.assert_(self.yes_no_required_allow.required, assert_msg)
        self.assert_(self.yes_no_required_allow.allow_adds, assert_msg)
        self.assertEqual(len(self.yes_no_required_allow.categories), 2, assert_msg)
        self.assertEqual(self.yes_no_required_allow.categories["yes"], 3, assert_msg)
        self.assertEqual(self.yes_no_required_allow.categories["no"], 2, assert_msg)
        
        assert_msg = "yes_no_required_noallow"
        self.assert_(not self.yes_no_required_noallow.allow_adds, assert_msg)

        assert_msg = "ynu_allow"
        self.assert_(self.ynu_allow.allow_adds, assert_msg)
        self.assertEqual(len(self.ynu_allow.categories), 0, assert_msg)
            
    def test_load_category_stats(self):
        f = open(self.yes_no_path, "r")
        stats = json.load(f)
        f.close()
        
        self.assertEquals(len(stats), 2, "loading the yes_no .json file")
        self.assertEquals(stats["yes"], 3, "loading the yes_no .json file")
        self.assertEquals(stats["no"], 2, "loading the yes_no .json file")
        
    def test_store_category_stats(self):
        assert_msg = "empty dict"
        self.assertEquals(self.ynu_allow.categories, {}, assert_msg)
        self.ynu_allow.store_category_stats() # write out the stats
        self.assert_(os.path.exists(self.ynu_path), assert_msg)
        
        self.ynu_allow.categories = {}        # clear stats
        self.ynu_allow.load_category_stats()  # reload stats
        self.assertEqual(len(self.ynu_allow.categories), 0, assert_msg)
        
        self.ynu_allow.is_valid("y")
        self.ynu_allow.is_valid("y")
        self.ynu_allow.is_valid("y")
        self.ynu_allow.is_valid("n")
        self.ynu_allow.is_valid("n")
        self.ynu_allow.is_valid("u")
        self.ynu_allow.store_category_stats() # write out the stats
        
        self.ynu_allow.categories = {}        # clear stats
        self.ynu_allow.load_category_stats()  # reload stats
        self.assertEqual(len(self.ynu_allow.categories), 3, assert_msg)
        self.assertEqual(self.ynu_allow.categories["y"], 3, assert_msg)
        self.assertEqual(self.ynu_allow.categories["n"], 2, assert_msg)
        self.assertEqual(self.ynu_allow.categories["u"], 1, assert_msg)
        
    def test_parse(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            self.yes_no_required_allow.reset()
            assert_msg = "prior to any parsing (strictness=%d)" % strictness
            self.assertEqual(self.yes_no_required_allow.text_len_lwm, None, assert_msg)
            self.assertEqual(self.yes_no_required_allow.text_len_hwm, None, assert_msg)

            yes_cnt = self.yes_no_required_allow.categories["yes"]
            assert_msg = "parse an already existing category (strictness=%d)" % strictness
            valid, msg = self.yes_no_required_allow.is_valid("yes", strictness)
            self.assert_(valid, assert_msg)
            self.assertEqual(self.yes_no_required_allow.categories["yes"],
                             yes_cnt + 1, assert_msg)
            self.assertEqual(self.yes_no_required_allow.text_len_lwm, 3, assert_msg)
            self.assertEqual(self.yes_no_required_allow.text_len_hwm, 3, assert_msg)
        
            assert_msg = "parse a new category (strictness=%d)" % strictness
            valid, msg = self.yes_no_required_allow.is_valid("new category", strictness)
            self.assert_(valid, assert_msg)
            self.assertEqual(self.yes_no_required_allow.categories["new category"],
                             1, assert_msg)
            self.assertEqual(self.yes_no_required_allow.text_len_lwm, 3, assert_msg)
            self.assertEqual(self.yes_no_required_allow.text_len_hwm, 12, assert_msg)
 
    def test_required(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            self.yes_no_required_allow.reset()
            self.ynu_allow.reset()
            
            assert_msg = "null string for a required field (strictness=%d)" % strictness
            valid, msg = self.yes_no_required_allow.is_valid(None, strictness)
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
                self.assertEqual(self.yes_no_required_allow.text_len_lwm, None, assert_msg)
                self.assertEqual(self.yes_no_required_allow.text_len_hwm, None, assert_msg)
            else:
                self.assert_(valid, assert_msg)
                
            assert_msg = "whitespace string for a required field (strictness=%d)" % strictness
            valid, msg = self.yes_no_required_allow.is_valid("  ", strictness)
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
                self.assertEqual(self.yes_no_required_allow.text_len_lwm, None, assert_msg)
                self.assertEqual(self.yes_no_required_allow.text_len_hwm, None, assert_msg)
            else:
                self.assert_(valid, assert_msg)

            num_categories = len(self.ynu_allow.categories)
            assert_msg = "null string for a not required field (strictness=%d)" % strictness
            valid, msg = self.ynu_allow.is_valid(None, strictness)
            self.assert_(valid, assert_msg)
            self.assertEqual(self.ynu_allow.text_len_lwm, 0, assert_msg)
            self.assertEqual(self.ynu_allow.text_len_hwm, 0, assert_msg)
            self.assertEqual(len(self.ynu_allow.categories), num_categories, assert_msg)

            num_categories = len(self.ynu_allow.categories)
            assert_msg = "whitespace string for a not required field (strictness=%d)" % strictness
            valid, msg = self.ynu_allow.is_valid("  ", strictness)
            self.assert_(valid, assert_msg)
            self.assertEqual(self.ynu_allow.text_len_lwm, 0, assert_msg)
            self.assertEqual(self.ynu_allow.text_len_hwm, 2, assert_msg)
            self.assertEqual(len(self.ynu_allow.categories), num_categories, assert_msg)
        
    def test_allow_adds(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            self.ynu_allow.reset()
            self.yes_no_required_noallow.reset()
            assert_msg = "add to a field that allows adds (strictness=%d)" % strictness
            num_categories = len(self.ynu_allow.categories)
            valid, msg = self.ynu_allow.is_valid("new category", strictness)
            self.assert_(valid, assert_msg)
            self.assertEqual(len(self.ynu_allow.categories), num_categories + 1,
                             assert_msg)
            self.assertEqual(self.ynu_allow.categories["new category"], 1,
                             assert_msg)
        
            assert_msg = "add to a field that doesn't allow adds (strictness=%d)" % strictness
            num_categories = len(self.yes_no_required_noallow.categories)
            valid, msg = self.yes_no_required_noallow.is_valid("new category", strictness)
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("not in vocabulary: value = "), assert_msg)
                self.assertEqual(len(self.yes_no_required_noallow.categories),
                                 num_categories, assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
    def test_merge(self):
        field1 = self.yes_no_required_noallow
        field1_yes = field1.categories["yes"]
        field1_no = field1.categories["no"]

        field2 = self.yes_no_required_allow
        valid, msg = field2.is_valid("yes")
        self.assert_(valid, "is_valid('yes') for field2")
        valid, msg = field2.is_valid("unknown")
        self.assert_(valid, "is_valid('unknown') for field2")
        field2_yes = field2.categories["yes"]
        field2_no = field2.categories["no"]
        field2_unknown = field2.categories["unknown"]

        field3 = self.ynu_allow
        
        self.assertEqual(len(field1.categories), 2, "prior to merge")
        field1.merge(field2)
        
        assert_msg = "after merge"
        self.assertEqual(len(field1.categories), 3, assert_msg)
        self.assertEqual(field1.categories["yes"], field1_yes + field2_yes, assert_msg)
        self.assertEqual(field1.categories["no"], field1_no + field2_no, assert_msg)
        self.assertEqual(field1.categories["unknown"], field2_unknown, assert_msg)
        
        field1.reset()
        assert_msg = "after merge with a field containing no categories"
        field1.merge(field3)
        self.assertEqual(len(field1.categories), 2, assert_msg)
        self.assertEqual(field1.categories["yes"], field1_yes, assert_msg)
        self.assertEqual(field1.categories["no"], field1_no, assert_msg)
        
    
    def test_reset(self):
        field = self.yes_no_required_allow
        orig_num_categories = len(field.categories)
        orig_yes = field.categories["yes"]
        orig_no = field.categories["no"]

        valid, msg = field.is_valid("yes")
        valid, msg = field.is_valid("unknown")
        assert_msg = "before call to reset stats"
        self.assertEqual(len(field.categories), orig_num_categories + 1, assert_msg)
        self.assertEqual(field.categories["yes"], orig_yes + 1, assert_msg)
        self.assertEqual(field.categories["unknown"], 1, assert_msg)
        self.assertEqual(field.text_len_lwm, 3, assert_msg)
        self.assertEqual(field.text_len_hwm, 7, assert_msg)
        
        field.reset()
        assert_msg = "after call to reset stats"
        self.assertEqual(len(field.categories), orig_num_categories, assert_msg)
        self.assertEqual(field.categories["yes"], orig_yes, assert_msg)
        self.assertEqual(field.categories["no"], orig_no, assert_msg)
        self.assert_(field.text_len_lwm is None, assert_msg)
        self.assert_(field.text_len_hwm is None, assert_msg)
 
    def test_update_watermarks(self):
        assert_msg = "prior to processing"
        self.assert_(self.ynu_allow.text_len_lwm is None, assert_msg)
        self.assert_(self.ynu_allow.text_len_hwm is None, assert_msg)
        
        assert_msg = "after processing whitespace"
        valid, msg = self.ynu_allow.is_valid("  ")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_lwm, 2, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_hwm, 2, assert_msg)

        assert_msg = "after processing null"
        valid, msg = self.ynu_allow.is_valid(None)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_hwm, 2, assert_msg)

        assert_msg = "after processing 'new category'"
        valid, msg = self.ynu_allow.is_valid("new category")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.ynu_allow.text_len_hwm, 12, assert_msg)
    
    def test_render_metadata(self):
        yes_no_required_allow_meta = self.yes_no_required_allow.render_metadata()
        yes_no_required_noallow_meta = self.yes_no_required_noallow.render_metadata()
        ynu_allow_meta = self.ynu_allow.render_metadata()

        self.assertEqual(yes_no_required_allow_meta[0],
                         "Category, required=True, calc_freq_cnts=True, " +
                            "file=%s, allow_adds=True" % self.yes_no_path,
                         "yes_no_required_allow")
        self.assertEqual(yes_no_required_noallow_meta[0],
                         "Category, required=True, calc_freq_cnts=True, " +
                            "file=%s, allow_adds=False" % self.yes_no_path,
                         "yes_no_required_noallow")
        self.assertEqual(ynu_allow_meta[0],
                         "Category, required=False, calc_freq_cnts=True, " +
                            "file=%s, allow_adds=True" % self.ynu_path,
                         "ynu_allow")
    
    def test_render_stats(self):
        stats = self.yes_no_required_allow.render_stats()
        assert_msg = "yes_no_categories"
        self.assertEqual(len(stats), 4, assert_msg)
        self.assertEqual(stats[1], "category_cnt: %d" % len(self.yes_no_required_allow.categories),
                         assert_msg)
        self.assertEqual(stats[2], "no", assert_msg)
        self.assertEqual(stats[3], "yes", assert_msg)
       
        stats = self.ynu_allow.render_stats()
        assert_msg = "ynu_categories"
        self.assertEqual(len(stats), 2, assert_msg)
        self.assertEqual(stats[1], "category_cnt: 0", assert_msg)
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
'''
Created on Jun 5, 2012

@author: rschaaf
'''

from checker import Field
import unittest

class FieldTest(unittest.TestCase):
    def setUp(self):
        self.required_freqcnts = Field("required_freqcnts", True, True)
        self.notrequired_nofreqcnts = Field("notrequired_nofreqcnts", False, False)

    def tearDown(self):
        pass

    def test_constructor(self):
        """Test constructor arguments"""        
        # "name" can be null or any string
        for name in ("Field A", " ", None):
            field = Field(name)
            self.assertEqual(field.name, name, "constructor: 'name' argument")
        
        # "required" is a boolean
        for required in (False, True):
            field = Field("name", required)
            self.assertEqual(field.required, required, "constructor: 'required' argument")

        # "calc_freq_cnts" is a boolean
        for calc_freq_cnts in (False, True):
            field = Field("name", True, calc_freq_cnts)
            self.assertEqual(field.calc_freq_cnts, calc_freq_cnts,
                             "constructor: 'calc_freq_cnts' argument")

    def test_constructor_defaults(self):
        """Test constructor argument defaults"""        
        field = Field("name")
        self.assert_(not field.required, "'required' defaults to 'False'")
        self.assert_(field.calc_freq_cnts, "'calc_freq_cnts' defaults to 'True'")       

    def test_merge(self):
        # merge two sets of null stats
        field_empty1 = Field("field_empty1")
        field_empty2 = Field("field_empty2")
        field_empty1.merge(field_empty2)
        assert_msg = "merge two sets of null stats"
        self.assert_(field_empty1.text_len_lwm is None, assert_msg)
        self.assert_(field_empty1.text_len_hwm is None, assert_msg)
        self.assertEqual(field_empty1.valid_cnt, 0, assert_msg)
        self.assertEqual(field_empty1.error_cnt, 0, assert_msg)
        self.assertEqual(field_empty1.empty_cnt, 0, assert_msg)
        self.assertEqual(len(field_empty1.freq_cnts), 0, assert_msg)
        self.assertEqual(len(field_empty1.errors), 0, assert_msg)

        assert_msg = "merge some stats with null stats"        
        field_some_stats = Field("field_some_stats")
        field_null_stats = Field("field_null_stats")
        valid, msg = field_some_stats.is_valid("abcd")
        field_some_stats.tally_valid("abcd")
        field_null_stats.merge(field_some_stats)
        self.assertEqual(field_null_stats.text_len_lwm, 4, assert_msg)
        self.assertEqual(field_null_stats.text_len_hwm, 4, assert_msg)
        self.assertEqual(len(field_null_stats.freq_cnts), 1, assert_msg)
        
        assert_msg = "merge null stats with some stats"        
        field_some_stats = Field("field_some_stats")
        field_null_stats = Field("field_null_stats")
        valid, msg = field_some_stats.is_valid("abcd")
        field_some_stats.tally_valid("abcd")
        field_some_stats.merge(field_null_stats)
        self.assertEqual(field_some_stats.text_len_lwm, 4, assert_msg)
        self.assertEqual(field_some_stats.text_len_hwm, 4, assert_msg)
        self.assertEqual(len(field_some_stats.freq_cnts), 1, assert_msg)

        assert_msg = "merge with lower-low and higher-high watermarks"
        field_higher_lwm_lower_hwm = Field("field_higher_lwm_lower_hwm")
        valid, msg = field_higher_lwm_lower_hwm.is_valid("abcd")
        field_higher_lwm_lower_hwm.tally_valid("abcd")
        field_lower_lwm_higher_hwm = Field("field_lower_lwm_higher_hwm")
        valid, msg = field_lower_lwm_higher_hwm.is_valid("abc")
        valid, msg = field_lower_lwm_higher_hwm.is_valid("abcde")
        field_lower_lwm_higher_hwm.tally_valid("abc")
        field_lower_lwm_higher_hwm.tally_valid("abcde")
        
        field_higher_lwm_lower_hwm.merge(field_lower_lwm_higher_hwm)
        self.assertEqual(field_higher_lwm_lower_hwm.text_len_lwm, 3, assert_msg)
        self.assertEqual(field_higher_lwm_lower_hwm.text_len_hwm, 5, assert_msg)
        self.assertEqual(len(field_higher_lwm_lower_hwm.freq_cnts), 3, assert_msg)
       
        assert_msg = "merge with higher-low and lower-high watermarks"
        field_higher_lwm_lower_hwm = Field("field_higher_lwm_lower_hwm")
        valid, msg = field_higher_lwm_lower_hwm.is_valid("abcd")
        field_higher_lwm_lower_hwm.tally_valid("abcd")
        field_lower_lwm_higher_hwm = Field("field_lower_lwm_higher_hwm")
        valid, msg = field_lower_lwm_higher_hwm.is_valid("abc")
        valid, msg = field_lower_lwm_higher_hwm.is_valid("abcde")
        field_lower_lwm_higher_hwm.tally_valid("abc")
        field_lower_lwm_higher_hwm.tally_valid("abcde")
        
        field_lower_lwm_higher_hwm.merge(field_higher_lwm_lower_hwm)
        self.assertEqual(field_lower_lwm_higher_hwm.text_len_lwm, 3, assert_msg)
        self.assertEqual(field_lower_lwm_higher_hwm.text_len_hwm, 5, assert_msg)
        self.assertEqual(len(field_lower_lwm_higher_hwm.freq_cnts), 3, assert_msg)
        
    def test_calc_freq_cnts(self):
        field_freq_cnts = Field("field_freq_cnts", True, True)
        field_freq_cnts.tally_valid("abcd")
        self.assertEqual(len(field_freq_cnts.freq_cnts), 1, "field_freq_cnts")

        field_no_freq_cnts = Field("field_no_freq_cnts", True, False)
        field_no_freq_cnts.tally_valid("abcd")
        self.assertEqual(len(field_no_freq_cnts.freq_cnts), 0, "no_field_freq_cnts")

    def test_is_valid(self):
        field_not_required = Field("name", False)
        field_required = Field("name", True)
        
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            assert_msg = "is_valid with a not required field and valid value (strictness=%d)"
            valid, err_msg = field_not_required.is_valid("valid value", strictness)
            self.assert_(valid, assert_msg % strictness)
            self.assert_(err_msg is None, assert_msg % strictness)
        
            assert_msg = "is_valid with a not required field and empty value (strictness=%d)"
            valid, err_msg = field_not_required.is_valid(" ", strictness)
            self.assert_(valid, assert_msg % strictness)
            self.assert_(err_msg is None, assert_msg % strictness)
        
            assert_msg = "is_valid:strict with a required field and valid value (strictness=%d)"
            valid, err_msg = field_required.is_valid("valid value", strictness)
            self.assert_(valid, assert_msg % strictness)
            self.assert_(err_msg is None, assert_msg % strictness)
        
            assert_msg = "is_valid:strict with a required field and empty value (strictness=%d)"
            valid, err_msg = field_required.is_valid(" ", strictness)
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg % strictness)
                self.assertEqual(err_msg, "required", assert_msg % strictness)
            else:
                self.assert_(valid, assert_msg % strictness)
                self.assert_(err_msg is None, assert_msg % strictness)
        
    def test_tally_valid(self):
        field = Field("name")
        self.assertEqual(field.valid_cnt, 0, "valid_cnt before first call to tally_valid")
        self.assertEqual(field.empty_cnt, 0, "empty_cnt before first call to tally_valid")
        self.assertEqual(field.error_cnt, 0, "error_cnt before first call to tally_valid")
        
        field.tally_valid("value1")
        self.assertEqual(field.valid_cnt, 1, "after one call to tally_valid")
        field.tally_valid("value2")
        self.assertEqual(field.valid_cnt, 2, "after two calls to tally_valid")
        field.tally_valid(" ")
        self.assertEqual(field.valid_cnt, 3, "call tally_valid with a blank string")
        self.assertEqual(field.empty_cnt, 1, "empty_cnt after calling tally_valid with blank string")
        field.tally_valid(None)
        self.assertEqual(field.valid_cnt, 4, "call tally_valid with a null value")
        self.assertEqual(field.empty_cnt, 2, "empty_cnt after calling tally_valid with null value")
        
        self.assertEqual(field.error_cnt, 0, "error_cnt for tally_valid")
        self.assertEqual(field.empty_cnt, 2, "empty_cnt for tally valid")
        
    def test_tally_error(self):
        field = Field("name")
        msg1 = "msg 1"
        msg2 = "msg 2"
        msg_whitespace = " "
        msg_empty = ""
        msg_null = None
        self.assertEqual(field.error_cnt, 0, "error_cnt before first call to tally_error")
        
        field.tally_error(msg1)
        self.assertEqual(field.error_cnt, 1, "error_cnt after first call to tally_error")
        self.assertEqual(len(field.errors), 1)
        self.assertEqual(field.errors[msg1], 1)
        
        field.tally_error(msg2)
        self.assertEqual(field.error_cnt, 2, "error_cnt after second call to tally_error")
        self.assertEqual(len(field.errors), 2)
        self.assertEqual(field.errors[msg2], 1)
       
        field.tally_error(msg1)
        self.assertEqual(field.error_cnt, 3, "error_cnt after calling tally_error twice with same message")
        self.assertEqual(len(field.errors), 2)
        self.assertEqual(field.errors[msg1], 2)
        
        field.tally_error(msg_whitespace)
        self.assertEqual(field.error_cnt, 4, "error_cnt after calling tally_error with whitespace message")
        self.assertEqual(len(field.errors), 3)
        self.assertEqual(field.errors[msg_whitespace], 1)
        
        field.tally_error(msg_empty)
        self.assertEqual(field.error_cnt, 5, "error_cnt after calling tally_error with empty message")
        self.assertEqual(len(field.errors), 4)
        self.assertEqual(field.errors[msg_empty], 1)
        
        field.tally_error(msg_null)
        self.assertEqual(field.error_cnt, 6, "error_cnt after calling tally_error with null message")
        self.assertEqual(len(field.errors), 5)
        self.assertEqual(field.errors[msg_null], 1)
    
    def test_render_metadata(self):
        field1 = Field("name1")
        field2 = Field("name2", True)
        self.assertEqual(field1.render_metadata()[0],
                         "Field, required=False, calc_freq_cnts=True",
                         "render_metadata where required is False")
        self.assertEqual(field2.render_metadata()[0],
                         "Field, required=True, calc_freq_cnts=True",
                         "render_metadata where required is True")
    
    def test_render_stats(self):
        field = Field("name")
        stats = field.render_stats()
        self.assertEqual(stats[0],
                         "valid: 0, empty: 0, errors: 0, len_lwm: None, len_hwm: None",
                         "stats prior to any processing")
        
        field.is_valid("value")
        field.tally_valid("value")
        field.is_valid(None)
        field.tally_valid(None)
        field.tally_error("msg")
        stats = field.render_stats()
        self.assertEqual(stats[0],
                         "valid: 2, empty: 1, errors: 1, len_lwm: 0, len_hwm: 5",
                         "stats after processing")
    
    def test_render_errors(self):
        field = Field("name")
        errs = field.render_errors()
        self.assertEqual(len(errs), 0, "no errors to render")
        
        field.tally_error("required")
        field.tally_error("not valid")
        field.tally_error("required")
        errs = field.render_errors()
        self.assertEqual(len(errs), 2, "two different error messages")
        self.assertEqual(errs[0], "not valid: 1")
        self.assertEqual(errs[1], "required: 2")
    
    def test_render_freq_cnts(self):
        field = Field("field", True, True)
        freq_cnts = field.render_freq_cnts()
        assert_msg = "freq_cnts empty"
        self.assertEqual(len(freq_cnts), 0, assert_msg)
        freq_cnts_lines = field.render_freq_cnts()
        self.assertEqual(len(freq_cnts_lines), 0, assert_msg)

        field.tally_valid("bcd")
        field.tally_valid("abc")
        freq_cnts = field.render_freq_cnts()
        assert_msg = "freq_cnts with two distinct items"
        self.assertEqual(len(freq_cnts), 2, assert_msg)
        freq_cnts_lines = field.render_freq_cnts()
        self.assertEqual(len(freq_cnts_lines), 2, assert_msg)
        self.assertEqual(freq_cnts_lines[0], "       1 abc", assert_msg)
        self.assertEqual(freq_cnts_lines[1], "       1 bcd", assert_msg)
                         
        field_no_freq_cnts = Field("field_no_freq_cnts", True, False)
        field_no_freq_cnts.tally_valid("bcd")
        field_no_freq_cnts.tally_valid("abc")
        freq_cnts = field_no_freq_cnts.render_freq_cnts()
        assert_msg = "freq_cnts disabled for field"
        self.assertEqual(len(freq_cnts), 0, assert_msg)
        freq_cnts_lines = field_no_freq_cnts.render_freq_cnts()
        self.assertEqual(len(freq_cnts_lines), 0, assert_msg)
        
        freq_cnts = field.render_freq_cnts()
        self.assertEqual(len(freq_cnts), 2)
        
        field.tally_error("required")
        field.tally_error("not valid")
        field.tally_error("required")
        errs = field.render_errors()
        self.assertEqual(len(errs), 2, "two different error messages")
        self.assertEqual(errs[0], "not valid: 1")
        self.assertEqual(errs[1], "required: 2")
    
    def test_is_empty(self):
        field = Field("name")
        self.assert_(not field.is_empty("not empty"), "is_empty with non-empty value")
        self.assert_(field.is_empty("  "), "is_empty with whitespace value")
        self.assert_(field.is_empty(""), "is_empty with empty string")
        self.assert_(field.is_empty(None), "is_empty with None")
        
    def test_reset(self):
        field = Field("name")
        field.tally_valid("")
        field.tally_error("msg1")
        self.assertEqual(field.valid_cnt, 1, "valid_cnt before call to reset")
        self.assertEqual(field.empty_cnt, 1, "empty_cnt before call to reset")
        self.assertEqual(field.error_cnt, 1, "error_cnt before call to reset")
        self.assertEqual(len(field.errors), 1, "length of errors dict before call to reset")
        self.assertEqual(len(field.freq_cnts), 1, "length of freq_cnts dict before call to reset")
        field.reset()
        self.assertEqual(field.valid_cnt, 0, "valid_cnt after call to reset")
        self.assertEqual(field.empty_cnt, 0, "empty_cnt after call to reset")
        self.assertEqual(field.error_cnt, 0, "error_cnt after call to reset")
        self.assertEqual(len(field.errors), 0, "length of errors dict after call to reset")
        self.assertEqual(len(field.freq_cnts), 0, "length of freq_cnts dict after call to reset")
        
    def test_update_text_len_watermarks(self):
        field = Field("name")
        self.assert_(field.text_len_lwm is None, "text_len_lwm before first call to update_text_len_watermarks")
        self.assert_(field.text_len_hwm is None, "text_len_hwm before first call to update_text_len_watermarks")
        
        field.is_valid("1234")  # process a four-character string
        self.assertEqual(field.text_len_lwm, 4, "text_len_lwm after processing 4-char string")
        self.assertEqual(field.text_len_hwm, 4, "text_len_hwm after processing 4-char string")

        field.is_valid("     ")  # process a five character whitespace string
        self.assertEqual(field.text_len_lwm, 4, "text_len_lwm after processing 5-char string")
        self.assertEqual(field.text_len_hwm, 5, "text_len_hwm after processing 5-char string")

        field.is_valid("")  # process an empty string
        self.assertEqual(field.text_len_lwm, 0, "text_len_lwm after processing an empty string")
        self.assertEqual(field.text_len_hwm, 5, "text_len_hwm after processing an empty string")

        field.is_valid(None)  # process a null string
        self.assertEqual(field.text_len_lwm, 0, "text_len_lwm after processing a null string")
        self.assertEqual(field.text_len_hwm, 5, "text_len_hwm after processing a null string")
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
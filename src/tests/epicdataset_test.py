'''
Created on Jun 10, 2012

@author: rschaaf
'''

from checker import EpicDataSet, Field
import logging
import unittest


class Test(unittest.TestCase):
    logging.basicConfig()
    
    def setUp(self):
#        self.dataset = EpicDataSet("epic test data", EpicDataSet.EPIC_FILE_PAT)
        self.dataset = EpicDataSet("^(?:.*/)*(epic.*)\.esp.\d{8}$")

    def tearDown(self):
        pass


    def testName(self):
        for table_type in ("mem", "vis", "soc", "imm", "all", "prb", "pro", "ord",
                           "res", "med", "prg"):
            filepath = "/srv/esp-data/fake/epic%s.esp.06022012" % table_type
            valid_cnt, err_cnt = self.dataset.check_table(filepath, Field.STRICT, False, True)
            print "Processed '%s': valid_rows=%d, error_rows=%d" % (filepath, valid_cnt, err_cnt)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
'''
@author: Jeff Andre
March 2, 2021

'''

from checker import EhrDataSet, Field
import logging
import unittest


class Test(unittest.TestCase):
    logging.basicConfig()
    
    def setUp(self):
#        self.dataset = EpicDataSet("epic test data", EpicDataSet.EPIC_FILE_PAT)
        self.dataset = EhrDataSet("^(?:.*/)*(ehr.*)\.gwu.\d{8}$")

    def tearDown(self):
        pass


    def testName(self):
        for table_type in ("lab"):
            filepath = "/srv/esp/gwu/data/ehr%s.gwu.03022021" % table_type
            valid_cnt, err_cnt = self.dataset.check_table(filepath, Field.STRICT, False, True)
            print "Processed '%s': valid_rows=%d, error_rows=%d" % (filepath, valid_cnt, err_cnt)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

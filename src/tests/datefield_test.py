'''
Created on Jun 5, 2012

@author: rschaaf
'''
from checker import DateField, Field
import unittest

class DateFieldTest(unittest.TestCase):

    def setUp(self):
        self.min_date = "19000101"
        self.max_date = "24991231"
        self.date_required = DateField("date_required", True)
        self.date_not_required = DateField("date_not_required", False)
        self.date_with_min = DateField("date_with_min", True, True, self.min_date)
        self.date_with_max = DateField("date_with_max", True, True, None, self.max_date)

    def tearDown(self):
        pass

    def test_constructor(self):
        self.assert_(self.date_required.required, "required")
        self.assert_(not self.date_not_required.required, "not required")
        self.assert_(self.date_required.min_date is None, "no min_date")
        self.assert_(self.date_required.max_date is None, "no max_date")
        self.assert_(self.date_required.date_lwm is None, "no date low watermark")
        self.assert_(self.date_required.date_hwm is None, "no date high watermark")
        
        self.assertEqual(self.date_with_min.min_date.strftime(DateField.DEFAULT_DATE_FMT),
                         self.min_date, "min_date specified")
        self.assertEqual(self.date_with_max.max_date.strftime(DateField.DEFAULT_DATE_FMT),
                         self.max_date, "max_date specified")
    
    def test_parse(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            for dt in ["2012-06-15 12:34:56", "20120615123456", "20120615", "201206"]:
                valid, msg = self.date_required.is_valid(dt, strictness)
                assert_msg = "parse a valid date: '%s' (strictness=%d)" % (dt, strictness)
                self.assert_(valid, assert_msg)
        
            for dt in ["06-15-2012", "2012", "Jan-15"]:
                valid, msg = self.date_required.is_valid(dt, strictness)
                assert_msg = "parse an invalid date: '%s' (strictness=%d)" % (dt, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg)
                    self.assert_(msg.startswith("date cannot be parsed: "), assert_msg)
                else:
                    self.assert_(valid, assert_msg)                
            
    def test_required(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.date_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for required date (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:                
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
            valid, msg = self.date_required.is_valid(None, strictness)
            assert_msg = "null value for required date (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:                
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
            valid, msg = self.date_not_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for not required date (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)

            valid, msg = self.date_not_required.is_valid(None, strictness)
            assert_msg = "null value for not required date (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
    
    def test_out_of_range(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.date_with_min.is_valid("19000101", strictness)
            assert_msg = "date is the minimum date (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.date_with_min.is_valid("18991231", strictness)
            assert_msg = "date less than the minimum date (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("date < minimum date: date: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)                

            valid, msg = self.date_with_max.is_valid("24991231", strictness)
            assert_msg = "date is the maximum date (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.date_with_max.is_valid("25000101", strictness)
            assert_msg = "date greater than the maximum date (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("date > maximum date: date: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)                
        
    def test_merge(self):
        date_20120601 = "20120601"
        date_20120602 = "20120602"
        date_20120603 = "20120603"
        date_20120604 = "20120604"
        date_20120605 = "20120605"
        datefield1 = DateField("datefield1", True)
        datefield2 = DateField("datefield2", True)
        valid, msg = datefield1.is_valid(date_20120601)
        datefield1.merge(datefield2)
        self.assertEqual(datefield1.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120601, "merge None with not-None value")
        self.assertEqual(datefield1.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120601, "merge None with not-None value")
        
        datefield1.reset()
        datefield2.reset()
        valid, msg = datefield2.is_valid(date_20120602)
        datefield1.merge(datefield2)
        self.assertEqual(datefield1.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120602, "merge not-None with None value")
        self.assertEqual(datefield1.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120602, "merge not-None with None value")
        
        datefield1.reset()
        datefield2.reset()
        valid, msg = datefield1.is_valid(date_20120602)
        valid, msg = datefield1.is_valid(date_20120604)
        valid, msg = datefield2.is_valid(date_20120603)
        datefield1.merge(datefield2)
        self.assertEqual(datefield1.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120602, "merge within-watermark values")
        self.assertEqual(datefield1.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120604, "merge within-watermark values")
        
        datefield1.reset()
        datefield2.reset()
        valid, msg = datefield1.is_valid(date_20120603)
        valid, msg = datefield2.is_valid(date_20120601)
        valid, msg = datefield2.is_valid(date_20120605)
        datefield1.merge(datefield2)
        self.assertEqual(datefield1.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120601, "merge outside-watermark values")
        self.assertEqual(datefield1.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_20120605, "merge outside-watermark values")
    
    def test_reset(self):
        date_06152012 = "20120615"
        assert_msg = "after processing 20120615"
        valid, msg = self.date_not_required.is_valid(date_06152012)
        self.assertEqual(self.date_not_required.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06152012, assert_msg)
        self.assertEqual(self.date_not_required.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06152012, assert_msg)
        
        self.date_not_required.reset()
        
        assert_msg = "after reset"
        self.assert_(self.date_not_required.date_lwm is None, assert_msg)
        self.assert_(self.date_not_required.date_hwm is None, assert_msg)

    def test_update_watermarks(self):
        assert_msg = "prior to processing"
        self.assert_(self.date_not_required.text_len_lwm is None, assert_msg)
        self.assert_(self.date_not_required.text_len_hwm is None, assert_msg)
        self.assert_(self.date_not_required.date_lwm is None, assert_msg)
        self.assert_(self.date_not_required.date_hwm is None, assert_msg)
        
        assert_msg = "after processing whitespace"
        valid, msg = self.date_not_required.is_valid("  ")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.date_not_required.text_len_lwm, 2, assert_msg)
        self.assertEqual(self.date_not_required.text_len_hwm, 2, assert_msg)
        self.assert_(self.date_not_required.date_lwm is None, assert_msg)
        self.assert_(self.date_not_required.date_hwm is None, assert_msg)
        
        assert_msg = "after processing null"
        valid, msg = self.date_not_required.is_valid(None)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.date_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.date_not_required.text_len_hwm, 2, assert_msg)
        self.assert_(self.date_not_required.date_lwm is None, assert_msg)
        self.assert_(self.date_not_required.date_hwm is None, assert_msg)
        
        date_06152012 = "20120615"
        assert_msg = "after processing 20120615"
        valid, msg = self.date_not_required.is_valid(date_06152012)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.date_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.date_not_required.text_len_hwm, 8, assert_msg)
        self.assertEqual(self.date_not_required.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06152012, assert_msg)
        self.assertEqual(self.date_not_required.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06152012, assert_msg)
                         
        date_06162012 = "20120616"
        assert_msg = "after processing 20120616"
        valid, msg = self.date_not_required.is_valid(date_06162012)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.date_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.date_not_required.text_len_hwm, 8, assert_msg)
        self.assertEqual(self.date_not_required.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06152012, assert_msg)
        self.assertEqual(self.date_not_required.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06162012, assert_msg)

        date_06142012 = "20120614"
        assert_msg = "after processing 20120614"
        valid, msg = self.date_not_required.is_valid(date_06142012)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.date_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.date_not_required.text_len_hwm, 8, assert_msg)
        self.assertEqual(self.date_not_required.date_lwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06142012, assert_msg)
        self.assertEqual(self.date_not_required.date_hwm.strftime(DateField.DEFAULT_DATE_FMT),
                         date_06162012, assert_msg)
    
    def test_render_metadata(self):
        date_not_required_meta = self.date_not_required.render_metadata()
        date_required_meta = self.date_required.render_metadata()
        date_with_min_meta = self.date_with_min.render_metadata()
        date_with_max_meta = self.date_with_max.render_metadata()

        self.assertEqual(date_not_required_meta[0],
                         "Date, required=False, calc_freq_cnts=True, " +
                            "min date=None, max date=None",
                         "date_not_required metadata")
        self.assertEqual(date_required_meta[0],
                         "Date, required=True, calc_freq_cnts=True, " +
                            "min date=None, max date=None",
                         "date_required metadata")

        self.assertEqual(date_with_min_meta[0],
                         "Date, required=True, calc_freq_cnts=True, " +
                            "min date=1900-01-01, max date=None",
                         "date_with_min metadata")

        self.assertEqual(date_with_max_meta[0],
                         "Date, required=True, calc_freq_cnts=True, " +
                            "min date=None, max date=2499-12-31",
                         "date_with_max metadata")
    
    def test_render_stats(self):
        stats = self.date_required.render_stats()
        self.assertEqual(stats[1], "date_lwm: None, date_hwm: None", "before any processing")
        
        valid, msg = self.date_required.is_valid("20120614")
        stats = self.date_required.render_stats()
        self.assertEqual(stats[1], "date_lwm: 2012-06-14, date_hwm: 2012-06-14", "after processing 20120614")
        
        valid, msg = self.date_required.is_valid("20120616")
        stats = self.date_required.render_stats()
        self.assertEqual(stats[1], "date_lwm: 2012-06-14, date_hwm: 2012-06-16", "after processing 20120616")
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
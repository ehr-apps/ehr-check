'''
Created on Jun 5, 2012

@author: rschaaf
'''
from checker import Field, NumberField
import unittest

class NumberFieldTest(unittest.TestCase):

    def setUp(self):
        self.min_number = 1
        self.max_number = 100
        self.pos_int_required = NumberField("pos_int_required", True, NumberField.POSINT)
        self.pos_int_not_required = NumberField("pos_int_not_required", False, NumberField.POSINT)
        self.int_required = NumberField("int_required", True, NumberField.INT)
        self.no_pat_required = NumberField("no_pat_required", True)
        self.pos_int_with_min = NumberField("pos_int_with_min", True, NumberField.POSINT, True, self.min_number)
        self.pos_int_with_max = NumberField("pos_int_with_max", True, NumberField.POSINT, True, None, self.max_number)
        self.float = NumberField("float", True, NumberField.FLOAT)

    def tearDown(self):
        pass

    def test_constructor(self):
        self.assert_(self.pos_int_required.required, "required")
        self.assert_(not self.pos_int_not_required.required, "not required")
        self.assert_(self.pos_int_required.min_number is None, "no min_number")
        self.assert_(self.pos_int_required.max_number is None, "no max_number")
        self.assert_(self.pos_int_required.number_lwm is None, "no date low watermark")
        self.assert_(self.pos_int_required.number_hwm is None, "no date high watermark")
        
        self.assertEqual(self.pos_int_with_min.min_number, self.min_number,
                         "min_number specified")
        self.assertEqual(self.pos_int_with_max.max_number, self.max_number,
                         "max_number specified")
        
        self.assert_(self.no_pat_required.number_format is None, "no number format")
        self.assertEqual(self.int_required.number_format, NumberField.INT, "INT as number format")
        self.assertEqual(self.pos_int_required.number_format, NumberField.POSINT, "POSINT as number format")
        self.assertEqual(self.float.number_format, NumberField.FLOAT, "FLOAT as number format")
    
    def test_parse_full_match(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            assert_msg1 = "parse a valid number when no pattern is specified (strictness=%d)" % strictness
            assert_msg2 = "parse a valid number (strictness=%d)" % strictness
            for num in ("0", "-1.23"):
                valid, msg = self.no_pat_required.is_valid(num, strictness)
                self.assert_(valid, assert_msg1)
                self.assert_(msg is None, assert_msg2)

            assert_msg1 = "parse an invalid number when no pattern is specified (strictness=%d)" % strictness
            assert_msg2 = "parse an invalid number (strictness=%d)" % strictness
            for num in ("-1.23.2",):
                valid, msg = self.no_pat_required.is_valid(num, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg1)
                    self.assert_(msg.startswith("number cannot be parsed: "), assert_msg2)
                else:
                    self.assert_(valid, assert_msg2)
            
            assert_msg = "parse a valid positive integer (strictness=%d)" % strictness
            for num in ("23", "+23"):
                valid, msg = self.pos_int_required.is_valid(num, strictness)
                self.assert_(valid, assert_msg)

            assert_msg = "parse an invalid positive integer (strictness=%d)" % strictness
            for num in (" 2.3", "-23"):
                valid, msg = self.pos_int_required.is_valid(num, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg)
                    self.assert_(msg.startswith("no match: value: "), assert_msg)
                else:
                    self.assert_(valid, assert_msg)

            assert_msg = "parse a valid integer (strictness=%d)" % strictness
            for num in ("-23",):
                valid, msg = self.int_required.is_valid(num, strictness)
                self.assert_(valid, assert_msg)

            assert_msg = "parse an invalid integer (strictness=%d)" % strictness
            for num in ("-23.",):
                valid, msg = self.pos_int_required.is_valid(num, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg)
                    self.assert_(msg.startswith("no match: value: "), assert_msg)
                else:
                    self.assert_(valid, assert_msg)

            assert_msg = "parse a valid float (strictness=%d)" % strictness
            for num in ("1", "+1", "-1", ".1", "0.1", "+.1", "+0.1", "-.1", "-0.1"):
                valid, msg = self.float.is_valid(num, strictness)
                self.assert_(valid, assert_msg)
        
            assert_msg = "parse an invalid float (strictness=%d)" % strictness
            for num in ("x1.0",):
                valid, msg = self.float.is_valid(num, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg)
                    self.assert_(msg.startswith("number cannot be parsed: "), assert_msg)
                else:
                    self.assert_(valid, assert_msg)

            assert_msg = "parse an invalid float with no number pattern (strictness=%d)" % strictness
            for num in ("x1.0",):
                valid, msg = self.no_pat_required.is_valid(num, strictness)
                if strictness >= Field.MODERATE:
                    self.assert_(not valid, assert_msg)
                    self.assert_(msg.startswith("number cannot be parsed: "), assert_msg)
                else:
                    self.assert_(valid, assert_msg)

    def test_parse_partial_match(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            assert_msg = "parse a partially matching number string (strictness=%d)" % strictness
            for str in ["-23x", "1..23"]:
                valid, msg = self.float.is_valid(str, strictness)
                self.assert_(valid, assert_msg)
    
    def test_required(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.pos_int_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for required number (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
                self.assertEqual(msg, "required", assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
            valid, msg = self.pos_int_required.is_valid(None, strictness)
            assert_msg = "null value for required number (strictness=%d)" % strictness
            if strictness >= Field.MODERATE:
                self.assert_(not valid, assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
            valid, msg = self.pos_int_not_required.is_valid(" ", strictness)
            assert_msg = "whitespace value for not required number (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)

            valid, msg = self.pos_int_not_required.is_valid(None, strictness)
            assert_msg = "null value for not required number (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
    
    def test_out_of_range(self):
        for strictness in [Field.LOOSE, Field.MODERATE, Field.STRICT]:
            valid, msg = self.pos_int_with_min.is_valid(str(self.min_number), strictness)
            assert_msg = "number is the minimum number (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.pos_int_with_min.is_valid(str(self.min_number - 1), strictness)
            assert_msg = "number less than the minimum number (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("number < min: value: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)

            valid, msg = self.pos_int_with_max.is_valid(str(self.max_number), strictness)
            assert_msg = "number is the maximum number (strictness=%d)" % strictness
            self.assert_(valid, assert_msg)
        
            valid, msg = self.pos_int_with_max.is_valid(str(self.max_number + 1), strictness)
            assert_msg = "number greater than the maximum number (strictness=%d)" % strictness
            if strictness >= Field.STRICT:
                self.assert_(not valid, assert_msg)
                self.assert_(msg.startswith("number > max: value: "), assert_msg)
            else:
                self.assert_(valid, assert_msg)
        
    def test_merge(self):
        field1 = NumberField("field1", True, NumberField.INT, False)
        field2 = NumberField("field2", True, NumberField.INT, False)
        valid, msg = field1.is_valid("10")
        field1.merge(field2)
        self.assertEqual(field1.number_lwm, 10, "merge None with not-None value")
        self.assertEqual(field1.number_hwm, 10, "merge None with not-None value")
        
        field1.reset()
        field2.reset()
        valid, msg = field2.is_valid("20")
        field1.merge(field2)
        self.assertEqual(field1.number_lwm, 20, "merge not-None with None value")
        self.assertEqual(field1.number_hwm, 20, "merge not-None with None value")
        
        field1.reset()
        field2.reset()
        valid, msg = field1.is_valid("10")
        valid, msg = field1.is_valid("20")
        valid, msg = field2.is_valid("15")
        field1.merge(field2)
        self.assertEqual(field1.number_lwm, 10, "merge within-watermark values")
        self.assertEqual(field1.number_hwm, 20, "merge within-watermark values")
        
        field1.reset()
        field2.reset()
        valid, msg = field1.is_valid("16")
        valid, msg = field2.is_valid("9")
        valid, msg = field2.is_valid("21")
        field1.merge(field2)
        self.assertEqual(field1.number_lwm, 9, "merge outside-watermark values")
        self.assertEqual(field1.number_hwm, 21, "merge outside-watermark values")
    
    def test_reset(self):
        assert_msg = "after processing '10'"
        valid, msg = self.pos_int_not_required.is_valid("10")
        self.assertEqual(self.pos_int_not_required.number_lwm, 10, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_hwm, 10, assert_msg)
        
        self.pos_int_not_required.reset()
        
        assert_msg = "after reset"
        self.assert_(self.pos_int_not_required.number_lwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.number_hwm is None, assert_msg)

    def test_update_watermarks(self):
        assert_msg = "prior to processing"
        self.assert_(self.pos_int_not_required.text_len_lwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.text_len_hwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.number_lwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.number_hwm is None, assert_msg)
        
        assert_msg = "after processing whitespace"
        valid, msg = self.pos_int_not_required.is_valid("  ")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_lwm, 2, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_hwm, 2, assert_msg)
        self.assert_(self.pos_int_not_required.number_lwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.number_hwm is None, assert_msg)
        
        assert_msg = "after processing null"
        valid, msg = self.pos_int_not_required.is_valid(None)
        self.assert_(valid, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_hwm, 2, assert_msg)
        self.assert_(self.pos_int_not_required.number_lwm is None, assert_msg)
        self.assert_(self.pos_int_not_required.number_hwm is None, assert_msg)
        
        assert_msg = "after processing '100'"
        valid, msg = self.pos_int_not_required.is_valid("100")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_hwm, 3, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_lwm, 100, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_hwm, 100, assert_msg)
                         
        assert_msg = "after processing '101'"
        valid, msg = self.pos_int_not_required.is_valid("101")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_hwm, 3, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_lwm, 100, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_hwm, 101, assert_msg)

        assert_msg = "after processing '99'"
        valid, msg = self.pos_int_not_required.is_valid("99")
        self.assert_(valid, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_lwm, 0, assert_msg)
        self.assertEqual(self.pos_int_not_required.text_len_hwm, 3, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_lwm, 99, assert_msg)
        self.assertEqual(self.pos_int_not_required.number_hwm, 101, assert_msg)
    
    def test_render_metadata(self):
        pos_int_required_meta = self.pos_int_required.render_metadata()
        pos_int_not_required_meta = self.pos_int_not_required.render_metadata()
        int_required_meta = self.int_required.render_metadata()
        no_pat_required_meta = self.no_pat_required.render_metadata()
        pos_int_with_min_meta = self.pos_int_with_min.render_metadata()
        pos_int_with_max_meta = self.pos_int_with_max.render_metadata()
        float_meta = self.float.render_metadata()

        self.assertEqual(pos_int_required_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format='%s', min=None, max=None" % NumberField.POSINT,
                         "pos_int_required metadata")
        
        self.assertEqual(pos_int_not_required_meta[0],
                         "Number, required=False, calc_freq_cnts=True, " +
                            "number format='%s', min=None, max=None" % NumberField.POSINT,
                         "pos_int_not_required metadata")

        self.assertEqual(int_required_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format='%s', min=None, max=None" % NumberField.INT,
                         "int_required metadata")

        self.assertEqual(no_pat_required_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format=None, min=None, max=None",
                         "no_pat_required metadata")

        self.assertEqual(pos_int_with_min_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format='%s', min=%s, max=None" % (NumberField.POSINT, self.min_number),
                         "pos_int_with_min metadata")

        self.assertEqual(pos_int_with_max_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format='%s', min=None, max=%s" % (NumberField.POSINT, self.max_number),
                         "pos_int_with_max metadata")

        self.assertEqual(float_meta[0],
                         "Number, required=True, calc_freq_cnts=True, " +
                            "number format='%s', min=None, max=None" % NumberField.FLOAT,
                         "float metadata")
    
    def test_render_stats(self):
        stats = self.pos_int_required.render_stats()
        self.assertEqual(stats[1], "number_lwm: None, number_hwm: None", "before any processing")
        
        valid, msg = self.pos_int_required.is_valid("10")
        stats = self.pos_int_required.render_stats()
        self.assertEqual(stats[1], "number_lwm: 10, number_hwm: 10", "after processing '10'")
        
        valid, msg = self.pos_int_required.is_valid("20")
        stats = self.pos_int_required.render_stats()
        self.assertEqual(stats[1], "number_lwm: 10, number_hwm: 20", "after processing '20'")
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
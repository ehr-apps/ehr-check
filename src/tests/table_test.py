'''
Created on Jun 9, 2012

@author: rschaaf
'''
from checker import CategoryField, DateField, NumberField, Table, TextField
import copy
import os.path
import tempfile
import unittest

class TableTest(unittest.TestCase):
    DATE_YYYYMMDD = "%Y%m%d"


    def setUp(self):
        self.category_path = tempfile.mktemp(".json", "category")
        if os.path.exists(self.category_path):
            os.remove(self.category_path)
            
        self.base_table = Table("base",
                                "Base table",
                                CategoryField("category", True, self.category_path, True, False),
                                DateField("date", True, False),
                                NumberField("number", False, None, False),
                                TextField("text", False, None, False))
                                
        self.table1 = copy.deepcopy(self.base_table)
        self.table1.name = "table1"
        self.table1.description = "Table 1"

    def tearDown(self):
        if os.path.exists(self.category_path):
            os.remove(self.category_path)

    def test_constructor(self):
        self.assertEqual(self.base_table.name, "base", "test setting of table.name")
        self.assertEqual(len(self.base_table.fields), 4, "test setting of table.fields")
    
    def test_process_valid_row(self):
        assert_msg = "prior to calling process_row()"
        self.assertEqual(self.base_table.row_num, 0, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 0, assert_msg)
        self.assertEqual(self.base_table.error_rows, 0, assert_msg)
        row_all_four_fields = ["yes", "20120601", "100", "some text"]
        
        self.base_table.process_row(row_all_four_fields)
        assert_msg = "after processing one valid row"
        self.assertEqual(self.base_table.row_num, 1, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 1, assert_msg)
        self.assertEqual(self.base_table.error_rows, 0, assert_msg)
        
        row_omit_optional_fields = ["no", "20120602"]
        self.base_table.process_row(row_omit_optional_fields)
        assert_msg = "after processing two valid rows"
        self.assertEqual(self.base_table.row_num, 2, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 2, assert_msg)
        self.assertEqual(self.base_table.error_rows, 0, assert_msg)
    
    def test_process_invalid_row(self):
        row_bad_date_format = ["yes", "2012-06-01", "100", "some_text"]
        row_missing_required_field = [" ", "20120601", "100", "some_text"]
        self.base_table.process_row(row_bad_date_format)
        self.base_table.process_row(row_missing_required_field)
        assert_msg = "after processing two invalid rows"
        self.assertEqual(self.base_table.row_num, 2, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 0, assert_msg)
        self.assertEqual(self.base_table.error_rows, 2, assert_msg)

    def test_reset_field_stats(self):
        self.base_table.process_row(["yes", "20120601"])
        self.assertEqual(self.base_table.fields[0].valid_cnt, 1,
                         "before call to reset_field_stats()")
        
        self.base_table.reset_field_stats()
        self.assertEqual(self.base_table.fields[0].valid_cnt, 0,
                         "after call to reset_field_stats()")
    
    def test_reset_row_stats(self):
        self.base_table.process_row(["yes", "20120601"])  # valid row
        self.base_table.process_row(["no"])               # invalid row
        assert_msg = "after processing one valid and one invalid row"
        self.assertEqual(self.base_table.row_num, 2, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 1, assert_msg)
        self.assertEqual(self.base_table.error_rows, 1, assert_msg)
        
        self.base_table.reset_row_stats()
        assert_msg = "after reset_row_stats()"
        self.assertEqual(self.base_table.row_num, 0, assert_msg)
        self.assertEqual(self.base_table.valid_rows, 0, assert_msg)
        self.assertEqual(self.base_table.error_rows, 0, assert_msg)
        
    def test_reset(self):
        self.base_table.process_row(["yes", "20120601"])
        assert_msg = "before call to reset()"
        self.assertEqual(self.base_table.valid_rows, 1, assert_msg)
        self.assertEqual(self.base_table.fields[0].valid_cnt, 1, assert_msg)
        
        self.base_table.reset()
        assert_msg = "after call to reset()"
        self.assertEqual(self.base_table.valid_rows, 0, assert_msg)
        self.assertEqual(self.base_table.fields[0].valid_cnt, 0, assert_msg)
    
    def test_update_stored_counts(self):
        self.assert_(not os.path.exists(self.category_path),
                     "category file should not exist yet")
        self.base_table.update_stored_counts()
        self.assert_(os.path.exists(self.category_path),
                     "check that category file exists")
    
    def test_merge(self):
        self.table1.process_row(["yes", "20120601"])
        assert_msg = "table1 after processing one valid row"
        self.assertEqual(self.table1.valid_rows, 1, assert_msg)
        self.assertEqual(self.table1.fields[1].valid_cnt, 1, assert_msg)
        
        assert_msg = "base_table before call to merge()"
        self.assertEqual(self.base_table.valid_rows, 0, assert_msg)
        self.assertEqual(self.base_table.fields[1].valid_cnt, 0, assert_msg)
        
        self.base_table.merge(self.table1)
        assert_msg = "base_table after call to merge()"
        self.assertEqual(self.base_table.valid_rows, 1, assert_msg)
        self.assertEqual(self.base_table.fields[1].valid_cnt, 1, assert_msg)
    
    def test_render_metadata(self):
        meta_with_fields = self.base_table.render_metadata(True, True)
        meta_without_fields = self.base_table.render_metadata(False)
        
        assert_msg = "meta_with_fields"
        self.assertEqual(len(meta_with_fields), 5, assert_msg)
        self.assertEqual(meta_with_fields[0], "Table 'base' (Base table)", assert_msg)
        self.assert_(meta_with_fields[1].startswith("  1: Field 'category', Category, required=True, calc_freq_cnts=False, file=/tmp/category"),
                     assert_msg)
        self.assertEqual(meta_with_fields[2],
                         "  2: Field 'date', Date, required=True, calc_freq_cnts=False, min date=None, max date=None",
                         assert_msg)
        self.assertEqual(meta_with_fields[3],
                         "  3: Field 'number', Number, required=False, calc_freq_cnts=False, number format=None, min=None, max=None",
                         assert_msg)
        self.assertEqual(meta_with_fields[4],
                         "  4: Field 'text', Text, required=False, calc_freq_cnts=False, text format=None, min len=None, max len=None",
                         assert_msg)
        
        assert_msg = "meta_without_fields"
        self.assertEqual(len(meta_without_fields), 1, assert_msg)
        self.assertEqual(meta_without_fields[0], meta_with_fields[0], assert_msg)
    
    def test_render_results(self):
        row_all_four_fields = ["yes", "20120601", "100", "some text"]
        row_omit_optional_fields = ["no", "20120602"]
        row_omit_required_field = ["yes"]
        self.base_table.process_row(row_all_four_fields)
        self.base_table.process_row(row_omit_optional_fields)
        self.base_table.process_row(row_omit_required_field)
        
        results = self.base_table.render_results(True, True, True, True)
        assert_msg = "render_results(True, True)"
        self.assertEqual(len(results), 32, assert_msg)
        self.assertEqual(results[0],  "Table 'base' (Base table)", assert_msg)
        self.assertEqual(results[1],  "  valid_rows=2, error_rows=1", assert_msg)
        self.assertEqual(results[2],  "", assert_msg)
        self.assertEqual(results[3],  "  1: Field 'category'", assert_msg)
        self.assert_(results[4].startswith("    Category, required=True, calc_freq_cnts=False, file=/tmp/category"), assert_msg)        
        self.assertEqual(results[5],  "    valid: 3, empty: 0, errors: 0, len_lwm: 2, len_hwm: 3", assert_msg)
        self.assertEqual(results[6],  "    category_cnt: 2", assert_msg)
        self.assertEqual(results[7],  "    no", assert_msg)
        self.assertEqual(results[8],  "    yes", assert_msg)
        self.assertEqual(results[9],  "", assert_msg)
        self.assertEqual(results[10],  "  No errors", assert_msg)
        self.assertEqual(results[11], "", assert_msg)
        self.assertEqual(results[12], "  2: Field 'date'", assert_msg)
        self.assertEqual(results[13], "    Date, required=True, calc_freq_cnts=False, min date=None, max date=None", assert_msg)
        self.assertEqual(results[14], "    valid: 2, empty: 0, errors: 1, len_lwm: 8, len_hwm: 8", assert_msg)
        self.assertEqual(results[15], "    date_lwm: 2012-06-01, date_hwm: 2012-06-02", assert_msg)
        self.assertEqual(results[16], "", assert_msg)
        self.assertEqual(results[17], "  Field errors", assert_msg)
        self.assertEqual(results[18], "    required: 1", assert_msg)
        self.assertEqual(results[19], "", assert_msg)
        self.assertEqual(results[20], "  3: Field 'number'", assert_msg)
        self.assertEqual(results[21], "    Number, required=False, calc_freq_cnts=False, number format=None, min=None, max=None", assert_msg)
        self.assertEqual(results[22], "    valid: 3, empty: 2, errors: 0, len_lwm: 0, len_hwm: 3", assert_msg)
        self.assertEqual(results[23], "    number_lwm: 100, number_hwm: 100", assert_msg)
        self.assertEqual(results[24], "", assert_msg)
        self.assertEqual(results[25], "  No errors", assert_msg)
        self.assertEqual(results[26], "", assert_msg)
        self.assertEqual(results[27], "  4: Field 'text'", assert_msg)
        self.assertEqual(results[28], "    Text, required=False, calc_freq_cnts=False, text format=None, min len=None, max len=None", assert_msg)
        self.assertEqual(results[29], "    valid: 3, empty: 2, errors: 0, len_lwm: 0, len_hwm: 9", assert_msg)
        self.assertEqual(results[30], "", assert_msg)
        self.assertEqual(results[31], "  No errors", assert_msg)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
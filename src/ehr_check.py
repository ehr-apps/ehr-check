'''
                                  ESP Health
                           Data Checker Framework


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics Inc  http://www.commoninf.com
@copyright: (c) 2021 Commonwealth Informatics Inc
@license: LGPL
'''

from checker import EhrDataSet, Field
import configparser
import logging
import optparse
import os.path
import sys

CONFIG_NAME = "ehr_check.ini"
DEFAULT_LOG_LEVEL = logging.INFO
VERSION = "1.0"

def load_config():
    """
    return a dictionary with keys of the form
    <section>.<option> and their corresponding values
    """
    app_dir = os.path.dirname(os.getcwd())
    filename = app_dir + os.sep + CONFIG_NAME
    if os.path.exists(filename):
        config = {}
        cp = configparser.ConfigParser()
        cp.read(filename)
        for sec in cp.sections():
            name = sec.lower()
            for opt in cp.options(sec):
                config[name + "." + opt.lower()] = cp.get(sec, opt, raw=True).strip()
    else:
        sys.exit('ini file does not exist')
    return config

def parse_args():
    """
    parse the command line arguments
    """
    parser = optparse.OptionParser(version='%prog version ' + VERSION)
    parser.set_usage("%prog [options] file ...")
    parser.add_option("-c", metavar="strict|moderate|loose",
                      choices=["strict", "moderate", "loose"],
                      help="Checking level", action="store", dest="checking")
    parser.add_option("-s", "--summary", help="Summarize files of the same type",
                      action="store_true", dest="summary")
    parser.add_option("-v", "--verbose",
                      help="Verbose output", action="store_true", dest="verbose")
    parser.add_option("-f", "--file", dest="filespec", metavar="FILESPEC",
                      help="file spec name")
    parser.set_defaults(checking="strict", summary=False, verbose=False)

    return parser.parse_args()

def get_logger(name, config):
    level = config["logging.loglevel"]
    if level == "CRITICAL":
        log_level = logging.CRITICAL
    elif level == "ERROR":
        log_level = logging.ERROR
    elif level == "WARNING":
        log_level = logging.WARNING
    elif level == "INFO":
        log_level = logging.INFO
    elif level == "DEBUG":
        log_level = logging.DEBUG
    else:
        log_level = DEFAULT_LOG_LEVEL
    
    logging.basicConfig(filename=config["logging.logfile"],
                        format=config["logging.logformat"],
                        level=log_level)
    return logging.getLogger(name)

def str_to_check_level(str):
    if str == "loose":
        return Field.LOOSE
    elif str == "moderate":
        return Field.MODERATE
    else:
        return Field.STRICT

if __name__ == '__main__':
    prog_name = sys.argv[0]    
    opts, args = parse_args()
    config = load_config()
    logger = get_logger(prog_name, config)
        
    logger.debug("Logging started")
    logger.debug("ehr_check version %s" % VERSION)
    logger.debug("Checking level = " + opts.checking)
    logger.debug("Verbose = %s" % opts.verbose)
    logger.debug("Summary = %s" % opts.summary)

    logger.debug("Current working directory = " + os.getcwd())
    logger.debug("Application top-level directory = " + os.path.dirname(os.getcwd()))
    logger.debug("Config = " + str(config))
    
    check_level = str_to_check_level(opts.checking)
    
    table_names = args
    app_dir = os.path.dirname(os.getcwd())
    if opts.filespec:
        filespec = app_dir + os.sep + opts.filespec
    else: 
        filespec = app_dir + os.sep + config["ehr.filespec"]
    data_set = EhrDataSet(config["ehr.filenamepattern"], filespec, table_names)
    for name in table_names:
        if not os.path.exists(name):
            name = config["ehr.datadir"] + name
        logger.info("Checking " + name)
        if (data_set.header_isvalid(name)):
            valid_rows, err_rows, e2err_rows, e1_dup, e2_dup = data_set.check_table(name,
                                                    str_to_check_level(opts.checking),
                                                    not opts.summary, opts.verbose)
            if valid_rows is not None:
                logger.info("  valid rows: %d, E1 error rows: %d, E2 error rows: %d" % (valid_rows, err_rows, e2err_rows))
                logger.info("  E1 duplicates: %d, E2 duplicates: %d" % (e1_dup, e2_dup))
        else:
            logger.info("Invalid header in %s" %(name))
            print("There are errors to correct. Invalid header in %s" %(name))
        
        if not opts.summary:
            data_set.reset_table_stats(name)

    if opts.summary:
        data_set.print_merged_table_stats(True, True, True, True, opts.verbose)
        
    logger.debug("Logging stopped")

# EHR Check

## Installation on Ubuntu (18.04, 20.04) or Windows 10

Requires python 3.6 or higher 


### Install ehr-check

1. Clone repo with the following command. 
```
git clone https://gitlab.com/ehr-apps/ehr-check.git
```

2. Change to project directory. 

cd ehr-check 

3. Configure settings. 
```
cp ehr_check.ini.sample ehr_check.ini
```

Set the following parameters in ehr_check.ini for your application. 
- datadir specifies where the data files are located. 
- filespec specifies the name of the file specification. 

4. Create data directory
```
mkdir /examplepath/data
```

### Run ehr-check

1. Put data file(s) in the data directory 

2. Edit the file specification as required. See example: all_labs_filespec_v1_0.csv. 

3. Run checker (examples) 

File specification name in ehr_check.ini
```
cd src
python3 ehr_check.py ehrlabs.gwu.04052021
```

File specification name with option (-f FILESPEC)
```
python3 ehr_check.py ehrmeds.gwu.06152021 -f meds_filespec_v1_0.csv
```

## Checking error results

Errors, if any, are printed to the terminal for fixing the data file.

Errors are logged (in /tmp/ehr_check.log) with the error, row number and level of error (E1, E2).
This log file can be used with a script for validating data before it's used in the end application.
E1 errors must be fixed, and E2 errors are warning level errors.


